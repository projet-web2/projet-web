<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('cookieconnect.php');

$articles1 = $bdd->prepare("SELECT * FROM articles WHERE categories = ? ORDER BY date_time_publi DESC LIMIT 3");
$articles1->execute(array('VOITURES'));
$articles2 = $bdd->prepare("SELECT * FROM articles WHERE categories = ? ORDER BY date_time_publi DESC LIMIT 3");
$articles2->execute(array('MULTIMEDIAS'));
$articles3 = $bdd->prepare("SELECT * FROM articles WHERE categories = ? ORDER BY date_time_publi DESC LIMIT 3");
$articles3->execute(array('VETEMENTS'));
$articles4 = $bdd->prepare("SELECT * FROM articles WHERE categories = ? ORDER BY date_time_publi DESC LIMIT 3");
$articles4->execute(array('OCCASIONS'));

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Produit TyuiopCase</title>
    <link href="style.css" rel="stylesheet" >
    <link href="inscription.php">
    <link href="connexion.php">
    <link href="bdd-web.sql">
    <link href="deconnexion.php">
    <link href="editionprofil.php">
    <link href="profil.php">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

    <nav class="navbar navbar-dark bg-dark sticky-top" id="top">
        <h2><a class="navbar-brand" href="index.php">TyuiopCase Menu</a></h2>
        <div class="nav-item">
          <h5><a class="nav-link" href="inscription.php">S'inscrire</a></h5>
        </div>
        <div class="nav-item">
          <h5><a class="nav-link" href="connexion.php">Se connecter</a></h5>
        </div>
        <div class="nav-item">
          <h5><a class="invisible" href="deconnexion.php">Se déconnecter</a></h5>
        </div>
        <div class="nav-item">
          <h5><a class="invisible" href="<?= "/profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
        </div>
        <div class="nav-item">
          <h5><a class="invisible" href="annonces.php">Mes Annonces</a></h5>
        </div>
        <div class="nav-item">
          <h5><a class="invisible" href="addannonces.php">Ajouter des Annonces</a></h5>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <nav id="menu" navigation-menu>
            <br />
            <nav class="navbar navbar-dark bg-primary"></nav>
            <br />
            <div align="center">
              <h4 style="color: deepskyblue">Les Catégories</h4>
            </div>
            <br />
            <ul class="topLevelMenu">
              <li class="menuFooter">
                <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
              </li>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <li class="menuFooter">
                <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
              </li>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <li class="menuFooter">
                <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
              </li>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <li class="menuFooter">
                <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
              </li>
            </ul>
          </nav>
        </div>
      </nav>

      <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Votre profil</title>
</head>
<body>    
<div  style="background: orangered" id="top">

<p id="promo">Promo du jour</p>

</div>
<br>
<table>
<tr>
<td>
    <div style="padding-left: 10%">
        <div class="card" style="width: 18rem;" id="photodeux">
            <img class="card-img-top" src="https://cdn.pixabay.com/photo/2018/05/17/21/08/car-3409695_960_720.jpg">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h6><span class="text-muted font-weight-black"><del>7000€</del></span</h6></h6>
                <h5>Fiat Punto, <strong>5400€</strong></h5>

                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>
<td>
<div style="padding-left: 40%">
<div class="card" style="width: 18rem;" id="photodeux">
<img class="card-img-top" src="https://c2.lestechnophiles.com/www.numerama.com/content/uploads/2016/09/iphone-7-test-8.jpg?resize=1212,712">
<div class="card-body">
    <h5 class="card-title"></h5>
    <h6><span class="text-muted font-weight-black"><del>300€</del></span</h6></h6>
    <h5>Iphone 7, <strong>250€</strong></h5>
    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
</div>
</div>
</div>
</td>
<td>
    <div style="padding-left: 70%">
        <div class="card" style="width: 18rem;" id="photodeux">
            <img class="card-img-top" src="https://i.ebayimg.com/thumbs/images/g/LZIAAOSw8o1c7hxa/s-l225.jpg">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h6><span class="text-muted font-weight-black"><del>8000€</del></span></h6>
                <h5>Audi TT MK1, <strong>7200€</strong></h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>
<td>
<div style="padding-left: 100%">
    <div class="card" style="width: 18rem;" id="photodeux">
        <img class="card-img-top" src="https://i.ebayimg.com/thumbs/images/g/IIMAAOSw3AdejDGg/s-l225.jpg">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <h6><span class="text-muted font-weight-black"><del>100€</del></span</h6></h6>
            <h5>Samsung Galaxy A50, <strong>80€</strong></h5>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
            <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
        </div>
    </div>
</div>
</td>
</tr>
</table>
<br>
<div id="txtpromo">
    <a style="text-align: center" href="promo.html">Voir plus de promo</a>
</div>
<br>
<br>
<br>


<table id="catVoiture" width="100%" border="1" cellspacing="1" cellpadding="1">
    <tr>
        <td>
            <h1 style="text-align: center">Voitures</h1>
        </td>
    </tr>
</table>
<br>
<table>
<tr>
<td>
    <div style="padding-left: 25%">
        <div class="card" style="width: 25rem;" id="photodeux">
            <img class="card-img-top" src="https://cdn-fs.opisto.fr/2018_6/Vehicule-RENAULT-CLIO-II-PHASE-2-1-4-2002-428927f00ae72cbd09b591d92d47405ca79e703e5febe32fe7b25a077865c4b6.JPG">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h5>RENAULT CLIO 2 PHASE 2, 2000€</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>
<td>
    <div style="padding-left: 43%">
        <div class="card" style="width: 25rem;" id="photodeux">
            <img class="card-img-top" src="https://fs.opisto.fr/Pictures/5895/2020_3/Vehicule---28be07a0be5aa433869007f86e19bd11472967157c14c2a49e773d8525fc4df9.jpg">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h5>VOLKSWAGEN POLO 3 PHASE 1, 3500€</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>
<td>
    <div style="padding-left: 60%">
        <div class="card" style="width: 25rem;" id="photodeux">
            <img class="card-img-top" src="https://fs.opisto.fr/Pictures/4336/2020_3/Vehicule-RENAULT-LAGUNA-II-PHASE-2-1-6-2007-5d27f6a741a5377402a582526cb5ee709bbebfaaca6d833c208ce4f76733cc98.jpg">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h5>RENAULT LAGUNA 2 PHASE 2, 5000€</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>


</tr>
</table>
<br>
<table>
<tr>
    <td>
        <div style="padding-left: 25%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://fs.opisto.fr/Pictures/5895/2020_3/Vehicule-CITROEN-C4-1-6-2009-84e7e8b2df23106183b39371cbeda8e2439a338e0ca9a0f7dfa310295bb5432c.jpg">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>CITROEN C4 Diesel, 3400€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div style="padding-left: 43%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://fs.opisto.fr/Pictures/136/2020_3/Vehicule-PEUGEOT-807-PHASE-2-2-2009-724cfd3b5efe8a840abe04952539cd374f93acf5e4424176270ecc06a94045fd.JPG">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>PEUGEOT 807, 5500€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div style="padding-left: 60%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://fs.opisto.fr/Pictures/4795/2020_3/Vehicule-NISSAN-MICRA-III-1-2-2004-7de1ea1cefe57cbe1af27ce905c562de65501a142d495a3964f339e596616875.jpg">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>NISSAN MICRA 3 PHASE 2, 3800€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>


</tr>
</table>
<br>
<div align="center">
    <?php
        while($a1 = $articles1->fetch())
        {
    ?>
            
            
            <h5 class="card-title"><b><?= $a1['titre'] ?></b></h5>
                
                <h6>Prix : <?=$a1['prix']?> € </h6>
                
                <h6> Prix occasion : <?= $a1['prix_occasion'] ?> € </h6>
                <br />
                <img src="miniatures/<?= $a1['id'] ?>.jpg" width="200" />
                <br><br>
                Description : <?= $a1['contenu'] ?>
                <br><br>
                Annonce publié le : <?= $a1['date_time_publi'] ?> par 
                <?php 
                    if ($a1['id_vendeur'] != null) : 
                ?>
                        <a href="profil.php?id_membres=<?= $a1['id_vendeur'] ?>"><b><?= $a1['pseudo'] ?></b></a>
                <?php 
                    else : 
                ?>
                        <b><?= $a1['pseudo'] ?></b>
                <?php 
                    endif; 
                ?>.
                <br /><br />
                <a class="btn btn-primary" href="article_voiture.php?id=<?= $a1['id'] ?>">Voir l'article</a>
                <br />
                -----------------------------
                <br /><br />
            
    <?php
        }
    ?>
</div>
<div align="center">
    <?php
        while($a1 = $articles1->fetch())
        {
    ?>
 
            <h5 class="card-title"><b><?= $a1['titre'] ?></b></h5>
                
                <h6>Prix : <?=$a1['prix']?> € </h6>
                
                <h6> Prix occasion : <?= $a1['prix_occasion'] ?> € </h6
                Description : <?= $a1['contenu'] ?>
                <br />
                <img src="miniatures/<?= $a1['id'] ?>.jpg" width="50" />
                <br />
                Annonce publié le : <?= $a1['date_time_publi'] ?> par 
                <?php 
                    if ($a1['id_vendeur'] != null) : 
                ?>
                        <a href="profil.php?id_membres=<?= $a1['id_vendeur'] ?>"><b><?= $a1['pseudo'] ?></b></a>
                <?php 
                    else : 
                ?>
                        <b><?= $a1['pseudo'] ?></b>
                <?php 
                    endif; 
                ?>.
                <br /><br />
                <a class="btn btn-primary" href="article_voiture.php?id=<?= $a1['id'] ?>">Voir l'article</a>
                <br />
                -----------------------------
                <br /><br />
            
    <?php
        }
    ?>
</div>
<br>
<div id="txtvoiture">
<a style="text-align: center" href="voiture.php">Voir plus</a>
</div>
<br>
<br>
<table id="catMultimedia" width="100%" border="1" cellspacing="1" cellpadding="1">
<tr>
<td>
    <h1 style="text-align: center">Multimédias</h1>
</td>
</tr>
</table>
<br>
<table>
<tr>
<td>
    <div style="padding-left: 25%">
        <div class="card" style="width: 25rem;" id="photodeux">
            <img class="card-img-top" src="https://images-na.ssl-images-amazon.com/images/I/41IwuHDqOPL._AC_SY400_.jpg" height="230px">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h5>LESHP Vidéoprojecteur vidéo multimédia à LED multimédias 1080P HD, 150€</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>
<td>
    <div style="padding-left: 43%">
        <div class="card" style="width: 25rem;" id="photodeux">
            <img class="card-img-top" src="https://m.media-amazon.com/images/I/81sc-FHsGYL._AC_UY327_FMwebp_QL65_.jpg">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h5>Câble HDMI CL3 2.0 haut débit 7,6 m, 10€</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>
<td>
    <div style="padding-left: 60%">
        <div class="card" style="width: 25rem;" id="photodeux">
            <img class="card-img-top" src="https://m.media-amazon.com/images/I/91hDu6Pbi+L._AC_UY327_FMwebp_QL65_.jpg" height="280px">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <h5>HP 27w Ecran PC Full HD 27" Noir, 100€</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
            </div>
        </div>
    </div>
</td>


</tr>
</table>
<br>
<table>
<tr>
    <td>
        <div style="padding-left: 25%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://m.media-amazon.com/images/I/81IEOd-7PxL._AC_UY327_FMwebp_QL65_.jpg" height="270px">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>Enceintes pour ordinateur alimentées par USB avec son dynamique , 40€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div style="padding-left: 43%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://www.yonis-shop.com/154122-large_default/support-voiture-telephone-iphone-galaxy-huawei-lg-htc-abs-60-95-mm-rouge.jpg" height="270px">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>Support Voiture Téléphone Stabilisateur Rouge, 5€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>            <td>
        <div style="padding-left: 60%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://images-eu.ssl-images-amazon.com/images/I/411+KzRcgCL._AC_US300_FMwebp_QL65_.jpg" height="275px">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>Apple iPad mini 3 64 Go Wi-Fi Argent, 350€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>
</tr>
</table>
<br>
<div align="center">
    <?php
        while($a2 = $articles2->fetch())
        {
    ?>
            
            <h5 class="card-title"><b><?= $a2['titre'] ?></b></h5>
                
                <h6>Prix : <?=$a2['prix']?> €</h6> 
                <h6>Prix occasion : <?= $a2['prix_occasion'] ?> €</h6>
                <br>
                <img src="miniatures/<?= $a2['id'] ?>.jpg" width="200" />                
                <br /><br>
                Description : <?= $a2['contenu'] ?>
                <br /><br>
                Annonce publié le : <?= $a2['date_time_publi'] ?> par 
                <?php 
                    if ($a2['id_vendeur'] != null) : 
                ?>
                        <a href="profil.php?id_membres=<?= $a2['id_vendeur'] ?>"><b><?= $a2['pseudo'] ?></b></a>
                <?php 
                    else : 
                ?>
                        <b><?= $a2['pseudo'] ?></b>
                <?php 
                    endif; 
                ?>.
                <br /><br />
                <a class="btn btn-primary" href="article_multimedia.php?id=<?= $a2['id'] ?>">Voir l'article</a>
                <br />
                -----------------------------
                <br /><br />
            
    <?php
        }
    ?>
</div>
<br>
<div id="txtvoiture">
<a style="text-align: center" href="multimedia.php">Voir plus</a>
</div>
<br>
<br>
<table id="catMultimedia" width="100%" border="1" cellspacing="1" cellpadding="1">
<tr>
    <td>
        <h1 style="text-align: center">Vêtements</h1>
    </td>
</tr>
</table>
<br>
<table>
<tr>
    <td>
        <div style="padding-left: 25%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://assets.adidas.com/images/w_600,f_auto,q_auto/f360ed1e708a419a9ff6aa480123ec72_9366/Chaussure_Advantage_Blanc_EF0213.jpg" height="298.49px">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>CHAUSSURE ADVANTAGE, 50€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div style="padding-left: 43%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/fr/dw53397f82/L1212_CAD_24.jpg?imwidth=914&impolicy=product" height="295.49px">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>Polo Lacoste rouge, 35€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div style="padding-left: 60%">
            <div class="card" style="width: 25rem;" id="photodeux">
                <img class="card-img-top" src="https://www.tocdemac.com/1557-home_default/tee-shirt-futur-marie-humour-mariage.jpg" height="298.49px">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <h5>tee-shirt futur mariés, 15€</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                    <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
    </td>


</tr>
</table>
<br>
<table>
    <tr>
        <td>
            <div style="padding-left: 25%">
                <div class="card" style="width: 25rem;" id="photodeux">
                    <img class="card-img-top" src="https://images.vinted.net/thumbs/f800/046a8_JYVm74VR6GCXjnEGeiX2rifD.jpeg?1542694972-6178c0a69bb68f2895e90bfdd7f07798d29c41fc" height="298.49px" width="398px">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h5>Manteau Yves Saint Laurent Hiver Laine et Cachemire , 450€</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                        <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div style="padding-left: 43%">
                <div class="card" style="width: 25rem;" id="photodeux">
                    <img class="card-img-top" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhMWFRUVFRUVFhYWFRcWFRcXFxUXGBUXFRUYHSggGBolHRcVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFRAQFysdFh0rLS0tLSstLSsuLS0tLS0tLS0tLS0tKy0tLS0tLTItLSs3LS0tLi0tKy0tKy0rLSstLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQBAgYFB//EAEIQAAIBAgMGAgYHBgQHAQAAAAABAgMRBCExBRJBUWFxE4EGIjKRofAHM0JScrHBFGKCktHhI6Ky8RclNENTg9IW/8QAFwEBAQEBAAAAAAAAAAAAAAAAAAECA//EACoRAQACAQIEBAYDAAAAAAAAAAABEQISIQMxQVETYaHRUnGBkbHhFCIy/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANakrHj4nHTu0nYD2geRh8XO9r3L++7ZhLTSmka+MiINFotPCaZsV4uzJKVaMvZd87ea1IJAAFAAAAAAAAAAAAAAAAAAAAAAAAAABrUV0yjON9VfvmegUEvhf8wksRiloreRneYbMJiEZuzO8yHF19yN7XfBLjy+LRpsqt4kFUaa3r2V7q19dF/sUWZu6Z4myNoblSVOWW9JyXe3rL4X950NjxdtbDVVOVN7lRZxfDeWl+XdfEkjoYyuZPL2JOo6UXVjuTtZxvpZ2vk9Ha66WPSiwttgAFAAAAAAAAAAAAAAAAAAAAAAAAClU9pr5zLpUrR9Zvml+oSUNjFzZs1aEIjxFPeWVn0btdcr8OBVo4WqlGEZOEIq32XKyVklZZdy84XMNtdijHhv70vexDe3rN+rbjrflkbwqKWnAzKD5ChrjsZGlTlUle0U3Zavkkub0N9j7QjWpqcdHmuxVxlJyi8rqzTjzXLucx6JbQ8Ou8Pd7k3J076qSzlG3DJN/w9TNq+gA1hK5sVQAAAAAAAAAAAAAAAAAAAAAAAArYiWZZbPOqzzCSw2LmlzKYRsc76W7WnQh4sU2oNXSdm7yivaXsr1nnzt1T6GL7fPyiKth4y5ZqzTSlFrlKLyer975llU+CxLnTjNpx3oxlZ6q6vZ9SR1l95e9FGls+MXvWTaVlklFLlGKyRZ3FmrZPXLg8giaFRPRp+ZDR2dRhUlVjBKpJWcl+i0T5vjZEOzsJGF0l6z9qXF/0RccuC82BLSqLe3b52vYnOWxGKdHEU/EtapJxjUWV3bKM197k1r0tY6eE7kWGwACgAAAAAAAAAAAAAAAAAAAACHEztHuebKRdxrzS6FKwRi5smFE2sRGUzfdRG2RYib3W+Su7Z+SNCxTnd2Xz+pP4ZxexqmJpYuqqu+6DhGcJJKUG2k5T31zd8uuSOooYuU0motJ6bzs/cr/AJixZq0L6StfXL325GKyko+pu3Wid0n0utCKo6ji92SUuHq5X1V76rgbbP33FOo1vcbKyXQCPGYClXjFVYX3ZRmldpxlHrFl+DtYqudpuPOO97nZ5+a9xV2jJpRqL7DvL8L1flZPtcyPbBFhqynFNEpWgAAAAAAAAAAAAAAAAAAADStKyYFPFTvn5FS5mpIjuRlvvDeNWa3AlTNosiubRLEh+zQ+7xvbO1+2hI526BM530n2tVoeHOnHevVjT3b7qblf2pLRJJZfvZ8C3Q6eEW87e/kTKn1s30KNLaUXZZuTSe6lfprpqn7ibxp8El1bv8EBLUo5ZO3zx5ladGT9Ru9001ayt3JnKfCz6O6z78vItIgrbMwao0404ttRVry1fL3LLyL8Xcp4m7TSbV1a61XbqUNi4+zlSk84PjxXBrmvnkFh7gMJmQoAAAAAAAAAAAAAAAAVsdLK3MslHaEs0ugFGRqjDkYbIy2bNbgWA3ibXNEbASJ/OiRpUw6d87X1Vk07aXjJNX6iLJIyNQNKGGjC9rtvNt6vgiW+dtTFSdot8kzjNqber0MdSpxpSqU6sYXl6zycnnBJNZRs+bEzQ7yEba5kkqySzaR5eHxkqns2UNL6ybu00uHnnqWoRS/q9f7gYeKpPTP8KaFDZ9PxFWtLetlfhfja2vcxTwi3rx04rn25IvsghxGKjC287NvL55FqMrlSpRjJWmk+Omj5rl3I8FGpGTi7OCzjJf6Wua9z6BXogj3yQKAAAAAAAAAAAAABwvpXtWeCxXjTTlhqygpNZunOKsnbk17/ACz7o8DbUIycozSnCWUk1dcmmhVrjMRO/JWw+KhUiqlOSnCSupRd0yZM+e7Q9EMVQk6mzMS6aebozbcPJtNPzV+pDH0z2hhv+twTlFa1KWluba3o/FGbrm6eDq/xN+kvpNzdM5HY/p9ga9kqvhyf2aq3PLe9l+86iFRNXTuuhbtzywyx2mKWEZuRbw3isJDaLI0zZMQJkyKWDg1b1t13vHee7nrly6Gyfzw4kikaGYQUUklZLKxrRrxk2k1la+fPQxXi3F21s7HM16OIWK8aNZeE4xXgqDc95J3Tjw53ur8+JJkdg6qS5IxCu3ord9fcUcHSftz9pq1m77q5cr58OSLqzFBSnNu0kkvP4FqxHDIr1selom76Ph7yCWrTk9Hu2fe/df3JlOy1KdOrOX7q97M1MJGSaldp9X+gFmOLjzXdFg8eOz3BPwpWfKd2verS8232NI7WnTdq1OVNL7ft0n/7I5xX41ELb2wQUMXGSTTVno000+zWTJwoAAAAAAADDPn219rPBYqdPE5UKs5TpVn7MXP1pwm+C3m8+HbT6BI8T0g2fCtBwqQU4tWcX8GuT6osDzMmrxas88s0+prvNcPccdW9GsZhG5bOr3hm/wBnrZw7RfD/AC9WyD/iBKi1DHYSrQd7b0VvU31Tdr/w7xqke/tT0XwWJu6lGKk/tw/w5+bj7Xnc55+h+OwnrbPxbcVn4VRpeSveDb7R7nUbJ9IMLiV/g1oTerje013g7SXuPTS5MzOMOuPGzx2u483D0fpBr4eSp7Qwsqb034KyfVRlk/4ZM6zZHpNhcR9TVjJ/dfqzX8ErP4FurGMouNSKlF6qSUovumcvtX6PsHV9aknQnqnTfqX4eo8l/DYzpmGtXCy5xpny3h2qqEkZnzNU9s4H2WsZSXDOc7dn69+zkensr6SMNN7leM8PNZNSTlFPldK680iX3SeBlzx/tHl7O8UyRS+c/lHm4HaNKtHepVITXOMlL32LiZpxmKWFU/r2+JIpFeLN1I0jepOybPKj6RUoVZUJzjGpGz3XJXcWrppeTPTm001LS2Z5lXY9OUlNqnvpWVV0061lot95ediSPQjVdRrhCyduLd3k+WmnUs7qas/mxBh6aglGPDm22+rfFm7l28zImpcidIqwqetbon8+4l3zcbiW4uR7w3zVCRQjyXXI3hJIrTqpK7aS5vJe88bH+l2BpX38VSutVGW/L+WF2Sa6tY4zPKLdKqiN0z53U+kKNXLA4aviXopKDjSXefDzsWvR6jtB1/2nEunSTWdGnm5K2SnJeqrX1vJ5aoxd8nSeHOMXlt+fs7oENDEKWXEmDmAxcyQYkVa0C2RziUeRiMIn07Hn4nANpppST1TWvdM6CdMhlSA+b7W9A8FVd/CdGazUqT3LPW+57N+trnnRwO1sH9TVjjKS+xUyqpdJN3fD7T00PqVSgnqrlOrgVwy/I1qKcbsX02oVH4ddPDVtHTq+qr8lNpe52fQ6jdWqPP236P0cRHdr0lO2SlpKP4ZLNdtDlHg9obO/6Z/teGX/AGpfWwXKLWfuTX7q1LtPJHeZlHaWycPXVq1KFTrKPrLtLVeTPN2B6YYXFWjGW5U40qlozvx3eEvLzsdB4nPP55j5kTMbw4rE/RzQ3t/DV6tCXDPfS7O6n/mMQ2Ptqj9Vi6daK4Teb/ni/wDUdqpLqvibxfVGdEO38jPrv893Gw29tql9bgY1FzptXf8ALOX5G0fpBrx+u2diIdoza/zU4nZ3fy0bxk+TGjzPFwnnhH0uHIU/pLofawuKX8Ef/osR+krBauNePO9P882jqY1H91/H+pv4r5P4jTPdNfD+H1cqvpPwPBVnloqfwtexpV+keMvqMHi6nallfleNzrfE/F8f6GVU7+ZNE9018P4fX9OVw/phi5K8NlYlt67/APhJdnOGa8kTLbu1Z+xs2MOtTEwfwVmdNv8AQzv9jWme54mPTCPX3ct/zyo9cFRXTxJyXk00zH/5vaFT6/atRLlQpQpe6Sf6HU+Jwv5cTRzWf6u3khpPFnpER9Icv/w/wbzxFTEYl861eXw3N09bBej2BpNeFhKSa+14alLr60k38S/42iVlLgktDWdVvLNc23YsYxHRJ4ueXPKU7qvlZLslrovyIZ1Hxlnq1HOyz59yGdXi8vu2V78mQVG+N7vV3S8rcSuaZYtppxyet27vXS3DI6elU3oqS4q5xUp9s+XwOn2HJ+DG/W3a7M5LD0AYuDCtwAUauJq4EgArSpEM6ReaNZQA8udIpV8LxR7dSkVqlIg4H0k9DcNirucfDq/+SFlJ/iWkvPPqjmXhts4D6uSxlFcHeU0uze+n0Tkuh9ar4a559XDtGoz7lPnmC+k6hfcxNGpQmtct9LuspL+U6LBel2AqW3cTTz0Upbkv5Z2Zf2js2jWW7XpQqL9+ClbtfQ5nH/RxgKl92M6TfGnN/wCme8vgbvGWd3X0sVCS9ScZdpJ/kTKWX65nzGt9ElH7GIqL8UIS/Kx9BwFLwqVOkndU6cIXtruRUb24aDboPQVR628rm6m9L+dyoqnzY3VT5zILSqPrbj1Ck+uemayK/i/N2FV+cyiw3wvzzuZvzstLf3KzqMxGTAt7/Xu1+ljWc1yWWnl77lZtmMwJpVut9ehFKstP7/DQ13DSVvnr/uRSpiOXErSm2XIYKpLSD465acM7F6jsF/bn5R5W5vTPuS4KeGmddseLjRgpKzzy7ybGF2fTp5xjnzeb8m9PItmZlW28DBkglABQAAAAAYaNJUyQAVZ0StVoHpNGkoAeHWwhUnhTopUSKeHJS2514dmkqLPflhCGWDFybPF8Nmd09V4M1eEGqSoeZfv7mZUu/uZ6P7Ix+yDVJph59/n/AHNrPkX1hDdYQapKh5yix4LfF+VuXbzPUjgyWODJcrs8ilgo62b01lJ6dG7F/D0lH2Ul2SX5F6GEJo4cUWhpk8UbxpG6iWmWiibKJtYyUaboNwAAAAAAAAAAAAAAYsLGQBrumNw3AEfhIx4SJQBF4KHgolAEXhI2VNG4A13EZ3TIAxYyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//2Q==" height="270px">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h5>Converse ALL STAR HI Blanc, 50€</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                        <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div style="padding-left: 60%">
                <div class="card" style="width: 25rem;" id="photodeux">
                    <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNoqtKIhqRMQFYF9qxMVHdHm6kspOGiAo-zCsBwOUZNGR6EoBn&usqp=CAU" height="275px">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h5>Chaussure New Balance MCHLAVEN 683301-60 vrg/gold, 100€</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                        <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<div align="center">
    <?php
        while($a3 = $articles3->fetch())
        {
    ?>
            
                <h5 class="card-title"><b><?= $a3['titre'] ?></b></h5>
                
                <h6>Prix : <?=$a3['prix']?> €</h6>
                <h6>Prix occasion : <?= $a3['prix_occasion'] ?> €</h6>
                <br>
                <img src="miniatures/<?= $a3['id'] ?>.jpg" width="200" />
                <br>
                Description : <?= $a3['contenu'] ?>
                <br>
                
                <br />
                Annonce publié le : <?= $a3['date_time_publi'] ?> par 
                <?php 
                    if ($a3['id_vendeur'] != null) : 
                ?>
                        <a href="profil.php?id_membres=<?= $a3['id_vendeur'] ?>"><b><?= $a3['pseudo'] ?></b></a>
                <?php 
                    else : 
                ?>
                        <b><?= $a3['pseudo'] ?></b>
                <?php 
                    endif; 
                ?>.
                <br /><br />
                <a class="btn btn-primary" href="article_vetement.php?id=<?= $a3['id'] ?>">Voir l'article</a>
                <br />
                -----------------------------
                <br /><br />
            
    <?php
        }
    ?>
</div>
<br>
<div id="txtvoiture">
    <a style="text-align: center" href="vetement.php">Voir plus</a>
</div>
<br>
<br>
<table id="catMultimedia" width="100%" border="1" cellspacing="1" cellpadding="1">
    <tr>
        <td>
            <h1 style="text-align: center">Occasions</h1>
        </td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td>
            <div style="padding-left: 25%">
                <div class="card" style="width: 25rem;" id="photodeux">
                    <img class="card-img-top" src="https://images.caradisiac.com/images/0/5/7/8/180578/S0-les-francais-n-ont-jamais-autant-achete-de-voitures-d-occasion-615411.jpg" height="298.49px">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h5>Renault clio 2 bleue, 1500€</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                        <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div style="padding-left: 43%">
                <div class="card" style="width: 25rem;" id="photodeux">
                    <img class="card-img-top" src="https://www.top-garage.fr/app/uploads/mariage-et-vo-065.jpg" height="295.49px">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h5>PEUGEOT Peugeot 206 Essence, 2000€</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                        <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div style="padding-left: 60%">
                <div class="card" style="width: 25rem;" id="photodeux">
                    <img class="card-img-top" src="https://static.boutique.orange.fr/media-cms/mediatheque/504x504-vue-1-noir-154030.png" height="298.49px">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h5>Ecouteurs sans fil JBL T120TW noir, 50€</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                        <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                    </div>
                </div>
            </div>
        </td>


    </tr>
</table>
    <br>
    <table>
        <tr>
            <td>
                <div style="padding-left: 25%">
                    <div class="card" style="width: 25rem;" id="photodeux">
                        <img class="card-img-top" src="https://www.cybertek.fr/images_produits/52816f51-8d3b-4332-bf68-5fe9f5b8871a.jpg" height="298.49px" width="398px">
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            <h5>SUPPORT POUR CASQUE AVEC SON SURROUND 7.1 , 45€</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                            <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div style="padding-left: 43%">
                    <div class="card" style="width: 25rem;" id="photodeux">
                        <img class="card-img-top" src="https://www.zokko.fr/205547-large_default/coque-iphone-11-pro-jpeux-pas-j-ai-netflix.jpg" height="300px">
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            <h5>Coque iPhone 11 PRO jpeux pas j'ai Netflix, 5€</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                            <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div style="padding-left: 60%">
                    <div class="card" style="width: 25rem;" id="photodeux">
                        <img class="card-img-top" src="https://304459-932169-raikfcquaxqncofqfm.stackpathdns.com/6919-home_default/chaussures-led-lumineuses.jpg" height="325px">
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            <h5>CHAUSSURES LUMINEUSES LED, 25€</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec venenatis leo, id ornare est. Sed rhoncus ligula eu quam..</p>
                            <a href="" class="btn btn-primary">EN SAVOIR PLUS</a>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <div align="center">
    <?php
        while($a4 = $articles4->fetch())
        {
    ?>
            
                <h5 class="card-title"><b><?= $a4['titre'] ?></b></h5>
                
                <h6>Prix : <?=$a4['prix']?> €</h6>
                <h6>Prix occasion : <?= $a4['prix_occasion'] ?> €</h6>
                <br>
                <img src="miniatures/<?= $a4['id'] ?>.jpg" width="200" />
                <br>
                Description : <?= $a4['contenu'] ?>                               
                <br><br>
                Annonce publié le : <?= $a4['date_time_publi'] ?> par 
                <?php 
                    if ($a4['id_vendeur'] != null) : 
                ?>
                        <a href="profil.php?id_membres=<?= $a4['id_vendeur'] ?>"><b><?= $a4['pseudo'] ?></b></a>
                <?php 
                    else : 
                ?>
                        <b><?= $a4['pseudo'] ?></b>
                <?php 
                    endif; 
                ?>.
                <br /><br />
                <a class="btn btn-primary" href="article_occasion.php?id=<?= $a4['id'] ?>">Voir l'article</a>
                <br />
                -----------------------------
                <br /><br />
            </li>
    <?php
        }
    ?>
</ul>
    <br>
    <div id="txtvoiture">
        <a style="text-align: center" href="occasion.php">Voir plus</a>
    </div>
    <br>
    <br>
</body>
</html>



