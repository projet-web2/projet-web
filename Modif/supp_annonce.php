<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('cookieconnect.php');

if(isset($_GET['id']) AND !empty($_GET['id']))
{
    $supp_id = htmlspecialchars($_GET['id']);
    
    $supp = $bdd->prepare("DELETE FROM articles WHERE id = ?");
    $supp->execute(array($supp_id));

    header("Location: annonces.php");
}

?>
