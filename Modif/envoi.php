<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

if(isset($_SESSION['id_membres']) AND !empty($_SESSION['id_membres']))
{

    if(isset($_POST['envoi_message']))
    {
        if(isset($_POST['destinataire'], $_POST['sujet'], $_POST['message']) AND !empty($_POST['destinataire']) AND !empty($_POST['sujet']) AND !empty($_POST['message']))
        {
            $destinataires = strtolower(htmlspecialchars($_POST['destinataire']));
            $sujet = htmlspecialchars($_POST['sujet']);
            $message = htmlspecialchars($_POST['message']);
            
            $id_destina = $bdd->prepare("SELECT id_membres FROM membres WHERE email = ?");
            $id_destina->execute(array($destinataires));
            $dest_exist = $id_destina->rowCount();

            if($dest_exist == 1)
            {
                $id_destina = $id_destina->fetch();
                $id_destina = $id_destina['id_membres'];

                $ins = $bdd->prepare("INSERT INTO message(id_expediteur, id_destinataire, sujet, message, date_time) VALUES (?, ?, ?, ?, NOW())");
                $ins->execute(array($_SESSION['id_membres'], $id_destina, $sujet, $message));

                $erreur = "Votre message a bien été envoyé";
            }
            else
            {
                $erreur = "L'utilisateur que vous avez chosi n'existe pas !";
            }
        }
        else
        {
            $erreur = "Merci de compléter tous les champs !";
        }
    }

    $destinataires = $bdd->query("SELECT email FROM membres ORDER BY email");

    if(isset($_GET['r']) AND !empty($_GET['r']))
    {
        $r = strtolower(htmlspecialchars($_GET['r']));
    }
    if(isset($_GET['o']) AND !empty($_GET['o']))
    {
        $o = urldecode($_GET['o']);
        $o = htmlspecialchars($_GET['o']);
        if(substr($o, 0,5) != 'RE : ')
        {
            $o = "RE : ".$o;
        }
    }

    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Envoyer un messages</title>
    </head>
    <body>
        <br>

        <a href="reception.php" class="btn btn-info btn-sm"> <i class="fa fa-envelope"></i> Voir ma boîte de reception</a>

        <br /><br /><br />


        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="" method="post">
          <fieldset>
            <legend class="text-center">Envoyer un message</legend>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Destinataire</label>
              <div class="col-md-9">
              <input type="email" placeholder="Adresse e-mail" name="destinataire" <?php if(isset($r))
                                                                                        {
                                                                                            echo 'value="'.$r.'"';
                                                                                        } ?> />
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Objet du mail</label>
              <div class="col-md-9">
              <input type="text" placeholder="Sujet de conversation" name="sujet" <?php if(isset($o))
                                                                                        {
                                                                                            echo 'value="'.$o.'"';
                                                                                        } ?> />
              </div>
            </div>
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Votre message</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="message" placeholder="Ecrivez votre message ici..." rows="5"></textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <input type="submit" class="btn btn-primary btn-sm" value="Envoyer" name="envoi_message"></input>
                
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>
        
    </body>
    </html>

<?php

}
else
{
    header("Location: connexion.php");
}

?>
