<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('cookieconnect.php');

$articles = $bdd->prepare("SELECT * FROM articles WHERE pseudo = ? ORDER BY date_time_publi DESC");
$articles->execute(array($_SESSION['pseudo']));

if(isset($_GET['r']) AND !empty($_GET['r']))
{
    $r = htmlspecialchars($_GET['r']);

    $r_array = explode(' ', $r);

    $articles = $bdd->query("SELECT * FROM articles WHERE titre LIKE '%".$r."%' AND pseudo = '$_SESSION[pseudo]' ORDER BY id DESC");
    if($articles->rowCount() == 0)
    {
        $articles = $bdd->query("SELECT * FROM articles WHERE CONCAT(titre, contenu) LIKE '%".$r."%' AND pseudo = '$_SESSION[pseudo]' ORDER BY id DESC");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="style.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link href="page accueil.html">
    

    <title>Mes Annonces</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark sticky-top">
      <h2><a class="navbar-brand" href="<?= "/monprofil.php?id_membres=".$_SESSION['id_membres'] ?>">TyuiopCase Menu</a></h2>

      <div class="nav-item">
        <h5><a class="invisible" href="annonces.php">Mes Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="addannonces.php">Ajouter des Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="editionprofil.php">Editer profil</a></h5>
      </div>
      <div class="invisible">
        <h5><a class="invisible" href="monprofil.php">Mon Profil</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="nav-link" href="deconnexion.php">Se Déconnecter</a></h5>
      </div>
</div>




      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
          <br />
          <nav class="navbar navbar-dark bg-primary"></nav>
          <br />
          <div align="center">
            <h4 style="color: deepskyblue">Les Catégories</h4>
          </div>
          <br />
          <ul class="topLevelMenu">
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
            </li>
          </ul>
        </nav>
      </div>
    </nav>
    <br />
    <div align="center">
    <form method="GET">
        <input type="search" name="r" placeholder="Recherche" />
        <input class="btn btn-success btn-sm"type="submit" value="Valider" />
    </form>
    <br />
    <a class="btn btn-primary btn-sm" href="addannonces.php"><i class="fa fa-plus"></i> Ajouter une annonce</a>
    <br /><br />
    <?php if($articles->rowCount() > 0)
        {
    ?>
    
        <ul>
            <?php
                while($a = $articles->fetch())
                {
            ?>
                    
                        <b><?= $a['titre'] ?></b>
                        <br /><br />
                        Prix : <?= $a['prix'] ?> € --- Prix occasion : <?= $a['prix_occasion'] ?> €
                        <br />
                        Catégorie : <?= $a['categories'] ?>
                        <br />
                        Description : <?= $a['contenu'] ?>
                        <br />
                        <img src="/miniatures/<?= $a['id'] ?>.jpg" width="50" />
                        <br />
                        Annonce publié le : <?= $a['date_time_publi'] ?> par 
                        <?php 
                            if ($a['id_vendeur'] != null) : 
                        ?>
                                <a href="profil.php?id_membres=<?= $a['id_vendeur'] ?>"><b><?= $a['pseudo'] ?></b></a>
                        <?php 
                            else : 
                        ?>
                                <b><?= $a['pseudo'] ?></b>
                        <?php 
                            endif; 
                        ?>.
                        <br /><br />
                        <a class="btn btn-info btn-sm"href="modif_annonce.php?edit=<?= $a['id'] ?>"><i class="far fa-edit"></i> Modifier l'annonce</a>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-danger btn-sm" href="supp_annonce.php?id=<?= $a['id'] ?>"><i class="fa fa-minus"></i> Supprimer l'annonce</a>
                        <br />
                        -----------------------------
                        <br /><br />
                    </li>
            <?php
                }
            ?>
        </ul>
    <?php
        }
        else
        {
    ?>
            Aucun résultat pour votre recherche.
    <?php
        }
    ?>
    <a class="btn btn-primary btn-sm" href=<?= "/monprofil.php?id_membres=".$_SESSION['id_membres'] ?> >Retour aux Annonces du site</a>
    </div>
</body>
</html>
