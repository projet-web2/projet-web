<link href="style.css" rel="stylesheet" >
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<nav class="navbar navbar-dark bg-dark sticky-top">
      <h2><a class="navbar-brand" href="index.php">TyuiopCase Menu</a></h2>

      <div class="nav-item">
        <h5><a class="nav-link" href="inscription.php">S'inscrire</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="connexion.php">Se connecter</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="deconnexion.php">Se déconnecter</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="<?= "/profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="mesannonces.php">Mes Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="addannonces.php">Ajouter des Annonces</a></h5>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
          <br />
          <nav class="navbar navbar-dark bg-primary"></nav>
          <br />
          <div align="center">
            <h4 style="color: deepskyblue">Les Catégories</h4>
          </div>
          <br />
          <ul class="topLevelMenu">
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
            </li>
          </ul>
        </nav>
      </div>
    </nav>

    <?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('cookieconnect.php');

if(isset($_POST['formconnect']))
{
    $emailconnect = htmlspecialchars($_POST['emailconnect']);
    $mdpconnect = ($_POST['mdpconnect']);

    $hashedpass = password_hash($mdpconnect, PASSWORD_DEFAULT);

    if(!empty($emailconnect) AND !empty($mdpconnect))
    {
        $requser = $bdd->prepare("SELECT * FROM membres WHERE email = :email");
        $requser->execute(array(':email' => $_POST['emailconnect']));
        $userexist = $requser->fetch();
        if($userexist && password_verify($_POST['mdpconnect'], $userexist['motdepasse']))
        {
            if(isset($_POST['souvenir']))
            {
                setcookie('email', $emailconnect, time()+365*24*3600, null, null, false, true);
                setcookie('password', $hashedpass = password_hash($mdpconnect, PASSWORD_DEFAULT), time()+365*24*3600, null, null, false, true);
            }
            $userinfo = $userexist;
            $_SESSION['id_membres'] = $userinfo['id_membres'];
            $_SESSION['pseudo'] = $userinfo['pseudo'];
            $_SESSION['nom'] = $userinfo['nom'];
            $_SESSION['prenom'] = $userinfo['prenom'];
            $_SESSION['email'] = $userinfo['email'];
            header("Location: profil.php?id_membres=".$_SESSION['id_membres']);
        }
        else
        {
            $erreur = "E-mail ou Mot de passe invalide !";
        }
    }
    else
    {
        $erreur = "Tous les champs doivent être complétés !";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
    
    <div align="center">
        <h2>Connexion</h2>

        <br /><br />
        
        <form method="POST" action="">

            <input type="email" name="emailconnect" placeholder="E-mail" />
            <input type="password" name="mdpconnect" placeholder="Mot de passe" />
            <br /><br />
            <input type="checkbox" name="souvenir" id="souvenircheckbox"/><label for="souvenircheckbox">Se souvenir de moi</label>
            <br /><br />
            <input  class="btn btn-primary"type="submit" name="formconnect" value="Connexion" />
            <br>
            <br>
            <div>
                <p><b>Vous avez oublié votre Mot de passe ?</b></p>
                <p>Suivez le lien et les instructions données :</p>
                <a href="http://projet-web.test/recuperation.php">Récupération</a>
            </div>
            -------------------------------------------------------------------------------------
            <div>
                <p><b>Vous n'êtes pas inscrit, inscrivez-vous !</b></p>
                <a href="inscription.php">Inscription</a>
            </div>
            
        </form>

        <br />

        <?php
            if(isset($erreur))
            {
                echo '<font color="red">'.$erreur.'</font>';
            }
        ?>

    </div>

</body>
</html>
