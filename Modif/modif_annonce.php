<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('cookieconnect.php');

$mode_edition = 0;

if(isset($_GET['edit']) AND !empty($_GET['edit']))
{
    $mode_edition = 1;

    $edit_id = htmlspecialchars($_GET['edit']);
    $edit_article = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
    $edit_article->execute(array($edit_id));
    
    if($edit_article->rowCount() == 1)
    {
        $edit_article = $edit_article->fetch();
    }
    else
    {
        die('Erreur ! L\'article que vous voulez voir n\'existe pas.');
    }
}

if(isset($_POST['article_titre'], $_POST['article_contenu'], $_POST['article_prix'], $_POST['article_prix_occasion']))
{
    if(!empty($_POST['article_titre']) AND !empty($_POST['article_contenu']) AND !empty($_POST['article_prix'])  AND !empty($_POST['article_prix_occasion']) )
    {
        $article_titre = htmlspecialchars($_POST['article_titre']);
        $article_contenu = htmlspecialchars($_POST['article_contenu']);
        $article_prix = htmlspecialchars($_POST['article_prix']);
        $article_prix_occasion = htmlspecialchars($_POST['article_prix_occasion']);
        $categories = htmlspecialchars($_POST['categories']);

        if (isset($_SESSION['id_membres']) && !empty($_SESSION['id_membres'])) {
            $idvendeur = $_SESSION['id_membres'];
            $pseudo = $_SESSION['pseudo'];
        } else {
            $idvendeur = null;
        }

        if($mode_edition == 0)
        {
            $ins = $bdd->prepare("INSERT INTO articles(titre, contenu, prix, prix_occasion, categories, pseudo, id_vendeur, date_time_publi) VALUES (?, ?, ?, ?, ?, ?, ?, NOW())");
            $ins->execute(array($article_titre, $article_contenu, $article_prix, $article_prix_occasion, $categories, $pseudo, $idvendeur));
            /* $last_id = $bdd->lastInsertId();
            
            if(isset($_FILES['miniature']) AND !empty($_FILES['miniature']['name']))
            {
                if(exif_imagetype($_FILES['miniature']['tmp_name']) == 2)      
                {
                    $chemin = "miniatures/".$last_id.".jpg";
                    move_uploaded_file($_FILES['miniature']['tmp_name'], $chemin);
                }
                else
                {
                    $erreur = "Votre Image de profil ne respecte pas les formats : .jpg, .jpeg";
                }
            } */

            $erreur = "Votre article a bien été publié !";
        }
        else
        {
            $update = $bdd->prepare("UPDATE articles SET titre = ?, contenu = ?, prix = ?, prix_occasion = ?, categories = ?, date_time_edition = NOW() WHERE id = ?");
            $update->execute(array($article_titre, $article_contenu, $article_prix, $article_prix_occasion, $categories, $edit_id));
            $erreur = "Votre article a bien été mise à jour !";
        }
    }
    else
    {
        $erreur = "Veuillez remplir tous les champs !";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="style.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link href="page accueil.html">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Edition de l'article</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark sticky-top">
      <h2><a class="navbar-brand" href="<?= "/monprofil.php?id_membres=".$_SESSION['id_membres'] ?>">TyuiopCase Menu</a></h2>

      <div class="nav-item">
        <h5><a class="invisible" href="annonces.php">Mes Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="addannonces.php">Ajouter des Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="editionprofil.php">Editer profil</a></h5>
      </div>
      <div class="invisible">
        <h5><a class="invisible" href="monprofil.php">Mon Profil</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="nav-link" href="deconnexion.php">Se Déconnecter</a></h5>
      </div>
</div>




      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
          <br />
          <nav class="navbar navbar-dark bg-primary"></nav>
          <br />
          <div align="center">
            <h4 style="color: deepskyblue">Les Catégories</h4>
          </div>
          <br />
          <ul class="topLevelMenu">
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
            </li>
          </ul>
        </nav>
      </div>
    </nav>
    <br>
    <div align="center">
    <form method="POST" enctype="multipart/form-data">
        <label>Titre de l'article : </label>
        <input type="text" name="article_titre" placeholder="Titre" <?php if($mode_edition == 1) { ?> value="<?= $edit_article['titre'] ?>"<?php } ?> />
        <br /><br />
        <label>Description : </label>
        <textarea name="article_contenu" placeholder="Descriptif de votre article"><?php if($mode_edition == 1) {  ?><?= $edit_article['contenu'] ?><?php } ?></textarea>
        <br /><br />
        <label>Photo : </label>
        <input type="file" name="miniature" <?php if($mode_edition == 1) { ?> /><?php } ?>
        <br /><br /><br />
        <label>Prix de l'article :</label>
        <input name="article_prix" placeholder="Prix de l'article" <?php if($mode_edition == 1) { ?> value="<?= $edit_article['prix'] ?>" <?php } ?> > € </input>
        <br /><br />
        <label>Prix occasion de l'article : </label>
        <input name="article_prix_occasion" placeholder="Prix occasion de l'article" <?php if($mode_edition == 1) { ?> value="<?= $edit_article['prix_occasion'] ?>" <?php } ?> > € </input>
        <p>Si l'article doit être une occasion, merci de saisir son prix d'occasion.
        <br />
        Mettre le prix puis le prix d'occasion (prix inférieur au prix de base) !
        <br />
        <b>Dans le cas contraire (SI ce n'est pas une occasion) saisir le même prix.</b>
        </p>
        <label>Catégories de l'article : </label>
        <select name="categories">
            <option value="VOITURES">Voitures</option>
            <option value="MULTIMEDIAS">Multimédias</option>
            <option value="VETEMENTS">Vêtements</option>
            <option value="OCCASIONS">Occasions</option>
        </select>
        <br /><br />
        <?php 
            if (isset($_SESSION['id_membres']) and !empty($_SESSION['id_membres'])) : 
        ?>
                <b><?= $_SESSION['pseudo'] ?></b>
        <?php 
            else : 
        ?>
        <?php 
            endif; 
        ?>
        <br /><br />
        <input class="btn btn-info btn-sm" type="submit" placeholder="Poster l'article" value="MISE À JOUR DE L'ARTICLE" />
    </form>
    <br />
    <?php
        if(isset($erreur))
        {
            echo '<font color="red">'.$erreur.'</font>';
        }
    ?>
    
    <a class="btn btn-primary btn-sm" href="annonces.php">Retour à mes Annonces</a>
    <br /><br />
    <a class="btn btn-primary btn-sm" href=<?= "/monprofil.php?id_membres=".$_SESSION['id_membres'] ?> >Retour aux Annonces du site</a>
    </div>
    <br>
    <br>
</body>
</html>
