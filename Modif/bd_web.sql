-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour espace_membre
CREATE DATABASE IF NOT EXISTS `espace_membre` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `espace_membre`;

-- Listage de la structure de la table espace_membre. discu
CREATE TABLE IF NOT EXISTS `discu` (
  `id_discu` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `pseudo` varchar(250) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id_discu`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.discu : ~9 rows (environ)
/*!40000 ALTER TABLE `discu` DISABLE KEYS */;
INSERT INTO `discu` (`id_discu`, `date`, `pseudo`, `message`) VALUES
	(15, NULL, 'Curseur', 'Salut'),
	(16, NULL, 'Sixte', 'Hello'),
	(18, NULL, 'Curseur', 'Salut, comment Ã§a va ?'),
	(19, NULL, 'Sixte', 'Coucou ! Je vais bien et toi?'),
	(20, NULL, 'Sixte', 'Tu fais quoi ici?'),
	(21, NULL, 'Curseur', 'Je vais bien, je te remercie.\r\nJe suis ici car je cherche une rÃ©ponse Ã  ma question'),
	(22, NULL, 'Sixte', 'C\'est quoi ta question? Peut Ãªtre que les personnes inscrites sur le site (et qui ont accÃ¨s au chan de discussion) pourront te rÃ©pondre !'),
	(23, NULL, 'Curseur', 'Je voudrais savoir comment mettre en place un systÃ¨me de rÃ©cupÃ©ration de mot de passe.');
/*!40000 ALTER TABLE `discu` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. membres
CREATE TABLE IF NOT EXISTS `membres` (
  `id_membres` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(250) DEFAULT NULL,
  `nom` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `motdepasse` text,
  `avatar` varchar(250) DEFAULT NULL,
  `confirmkey` varchar(250) DEFAULT NULL,
  `confirme` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_membres`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.membres : ~5 rows (environ)
/*!40000 ALTER TABLE `membres` DISABLE KEYS */;
INSERT INTO `membres` (`id_membres`, `pseudo`, `nom`, `prenom`, `email`, `motdepasse`, `avatar`, `confirmkey`, `confirme`) VALUES
	(1, 'Curseur', 'Malo', 'Valentin', 'kalki-wow@live.fr', '$2y$10$xPolgTRDbB6YEv8Hef85BOXnjgNmzI./FViYGSD5wlTQGImhUHE3C', '1.jpg', NULL, NULL),
	(2, 'Lhaurel', 'Malo', 'Baptiste', 'sixter-wow@live.fr', '$2y$10$XFe/PjpXlZl.RYZKnnR8xufduKWblDjSVAqxFhjdobmLmDHpTU8UC', 'default.png', NULL, NULL),
	(10, 'Cucu', 'MRTY', 'John', 'kalkivalentin117@gmail.com', '$2y$10$1BmXuZzZdlcOXS1N.R8/WO8tbHZJCw8gPXVNtpL3RzQ9EmtJ1axIi', 'default.png', NULL, NULL),
	(11, 'Test', 'Test', 'Test', 'test@gmail.com', '$2y$10$3wDXK6kjexNnmExj0KeKw.yb4mqhnYMl6JT5rURa2CuuX8updZH3C', '11.jpg', NULL, NULL),
	(12, 'Sixtte', 'Berthier', 'ValÃ©rie', 'sixte-wow@live.fr', '$2y$10$t6tX.jYCR8GqMrgm9uCHJOHymoGMtnRhStaInIWcZ8iJKeLE0gadm', '12.jpg', NULL, NULL),
	(15, 'Confirme', 'Conf', 'Irme', 'LhaurelXIII-wow@outlook.com', '$2y$10$niqt1SyjSm0DOR8OE2eIYO623hWG1vFeDvOrvZ/ihtAHOfxSgOj02', 'default.png', '07342321708445', 1);
/*!40000 ALTER TABLE `membres` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. message
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediteur` int(11) DEFAULT NULL,
  `id_destinataire` int(11) DEFAULT NULL,
  `sujet` varchar(250) DEFAULT NULL,
  `message` text,
  `lu` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.message : ~6 rows (environ)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`id`, `id_expediteur`, `id_destinataire`, `sujet`, `message`, `lu`, `date`) VALUES
	(6, 1, 2, 'Bonjour le nouveau', 'Salut !', 1, NULL),
	(12, 1, 2, 'Objet de test', 'Je te passe le Bonjour !\r\nSale humain !', 1, NULL),
	(13, 2, 1, 'RE : Objet de test', 'Coucou Ã  toi le mÃ©chant !', 1, NULL),
	(14, 2, 1, 'MÃ©chant 1', 'Pas beau', 1, NULL),
	(15, 1, 2, 'RE : RE : Objet de test', 'Cool !', 1, NULL);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. recuperation
CREATE TABLE IF NOT EXISTS `recuperation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `confirme` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.recuperation : ~0 rows (environ)
/*!40000 ALTER TABLE `recuperation` DISABLE KEYS */;
/*!40000 ALTER TABLE `recuperation` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
