<?php

if(!isset($_SESSION['id_membres']) AND isset($_COOKIE['email'], $_COOKIE['password']) AND !empty($_COOKIE['email']) AND !empty($_COOKIE['password']))
{
    $requser = $bdd->prepare("SELECT * FROM membres WHERE email = :email");
    $requser->execute(array(':email' => $_COOKIE['email']));
    $userexist = $requser->fetch();
    if($userexist && $_COOKIE['password'])
    {
        $userinfo = $userexist;
        $_SESSION['id_membres'] = $userinfo['id_membres'];
        $_SESSION['pseudo'] = $userinfo['pseudo'];
        $_SESSION['nom'] = $userinfo['nom'];
        $_SESSION['prenom'] = $userinfo['prenom'];
        $_SESSION['email'] = $userinfo['email'];
    }
}

?>
