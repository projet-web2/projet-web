<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');
if(isset($_SESSION['id_membres']) AND !empty($_SESSION['id_membres']))
{
    $msg = $bdd->prepare("SELECT * FROM message WHERE id_destinataire = ?  ORDER BY id DESC");
    $msg->execute(array($_SESSION['id_membres']));
    $msg_nbr = $msg->rowCount();

    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <title>Boîte de réception</title>
    </head>
    <body>
        
        <br />
        <a  class="btn btn-info btn-sm"href="profil.php?id_membres=<?= $_SESSION['id_membres'] ?>">Retour à mon Profil</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-success btn-sm" href="envoi.php">Nouveau message</a><br /><br />
        <h3>Ma Boîte de réception</h3>
        <?php 
            if($msg_nbr == 0)
            {
                echo "Vous n'avez aucun message pour le moment !";
            }
            while($m = $msg->fetch())
            {
                $pseudo_exp = $bdd->prepare("SELECT pseudo FROM membres WHERE id_membres = ?");
                $pseudo_exp->execute(array($m['id_expediteur']));
                $pseudo_exp = $pseudo_exp->fetch();
                $pseudo_exp = $pseudo_exp['pseudo'];
        ?>

                <?php if($m['lu'] == 1)
                    {
                ?>
                        <span style="color: grey">
                <?php
                    }
                ?>
                <a href="profil.php?id_membres=<?= $m['id_expediteur'] ?>"><b><?= $pseudo_exp ?></b></a> 
                vous a envoyé <a href="lecture.php?id=<?= $m['id'] ?>">un message</a>
                <br/>
                <b>Objet : </b><?= $m['sujet'] ?>. Envoyé le <?= $m['date_time'] ?>.
                <?php if($m['lu'] == 1)
                    {
                ?>
                        </span>
                <?php
                    }
                ?>
                &nbsp;&nbsp;&nbsp;
                <?php if($m['lu'] == 1)
                    {
                ?>
                        <span style="color: grey"><i>Lu</i></span>
                <?php
                    }
                    else
                    {
                ?>
                        <i>Non Lu</i>
                <?php
                    }
                ?>
                <br /><br />
                ---------------------------------------
                <br /><br />
        
        <?php
            }
        ?>

    </body>
    </html>

<?php

}

?>
