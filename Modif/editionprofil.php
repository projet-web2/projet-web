<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link href="index.php">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<nav class="navbar navbar-dark bg-dark sticky-top">
      <h2><a class="navbar-brand" href="#">TyuiopCase Menu</a></h2>

      <div class="nav-item">
        <h5><a class="nav-link" href="deconnexion.php">Se déconnecter</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="inscription.php">S'inscrire</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="connexion.php">Se connecter</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="<?= "/profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="mesannonces.php">Mes Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="invisible" href="addannonces.php">Ajouter des Annonces</a></h5>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
          <br />
          <nav class="navbar navbar-dark bg-primary"></nav>
          <br />
          <div align="center">
            <h4 style="color: deepskyblue">Les Catégories</h4>
          </div>
          <br />
          <ul class="topLevelMenu">
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
            </li>
          </ul>
        </nav>
      </div>
    </nav>


<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

if(isset($_SESSION['id_membres']))
{
    $changed = false;
    $requser = $bdd->prepare("SELECT * FROM membres WHERE id_membres = ?");
    $requser->execute(array($_SESSION['id_membres']));
    $user = $requser->fetch();

    if(isset($_FILES['avatar']) AND !empty($_FILES['avatar']['name']))
    {
        $taillemax = 2097152;
        $extenvalides = array('jpg', 'jpeg', 'png', 'gif');
        if($_FILES['avatar']['size'] <= $taillemax)
        {
            $extenupload = strtolower(substr(strrchr($_FILES['avatar']['name'], '.'), 1));
            if(in_array($extenupload, $extenvalides))
            {
                $destiavatar = "membres/avatars/".$_SESSION['id_membres'].".".$extenupload;
                $resultavatar = move_uploaded_file($_FILES['avatar']['tmp_name'], $destiavatar);
                if($resultavatar)
                {
                    $updateavatar = $bdd->prepare("UPDATE membres SET avatar = :avatar WHERE id_membres = :id_membres");
                    $updateavatar->execute(array(
                        'avatar' => $_SESSION['id_membres'].".".$extenupload,
                        'id_membres' => $_SESSION['id_membres']
                    ));
                    $changed = true;
                }
                else
                {
                    $erreur = "Erreur d'Upload de votre Image de profil";
                }
            }
            else
            {
                $erreur = "Votre Image de profil ne respecte pas les formats : .jpg, .jpeg, .png, .gif";
            }
        }
        else
        {
            $erreur = "Votre Image de profil ne doit pas dépasser 2Mo !";
        }
    }

    if(isset($_POST['newpseudo']) AND !empty($_POST['newpseudo']) AND $_POST['newpseudo'] != $user['pseudo'])
    {
        $newpseudo = htmlspecialchars($_POST['newpseudo']);
        $updatepseudo = $bdd->prepare("UPDATE membres SET pseudo = ? WHERE id_membres = ?");
        $updatepseudo->execute(array($newpseudo, $_SESSION['id_membres']));
        $changed = true;
    }

    if(isset($_POST['newnom']) AND !empty($_POST['newnom']) AND $_POST['newnom'] != $user['nom'])
    {
        $newnom = htmlspecialchars($_POST['newnom']);
        $updatenom = $bdd->prepare("UPDATE membres SET nom = ? WHERE id_membres = ?");
        $updatenom->execute(array($newnom, $_SESSION['id_membres']));
        $changed = true;
    }

    if(isset($_POST['newprenom']) AND !empty($_POST['newprenom']) AND $_POST['newprenom'] != $user['prenom'])
    {
        $newprenom = htmlspecialchars($_POST['newprenom']);
        $updateprenom = $bdd->prepare("UPDATE membres SET prenom = ? WHERE id_membres = ?");
        $updateprenom->execute(array($newprenom, $_SESSION['id_membres']));
        $changed = true;
    }

    if(isset($_POST['newemail']) AND !empty($_POST['newemail']) AND isset($_POST['newemail2']) AND !empty($_POST['newemail2']))
    {
        $email = htmlspecialchars($_POST['newemail']);
        $email2 = htmlspecialchars($_POST['newemail2']);

        if($email == $email2)
        {
            if(filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $reqemail = $bdd->prepare("SELECT email FROM membres WHERE email = ?");
                $reqemail->execute(array($email));
                $emailexist = $reqemail->rowCount();
                if($emailexist == 0)
                {
                    $updateemail = $bdd->prepare("UPDATE membres SET email = ? WHERE id_membres = ?");
                    $updateemail->execute(array($email, $_SESSION['id_membres']));
                    $changed = true;
                }
                else
                {
                    $erreur = "L'Adresse e-mail est déjà utilisée ! <br/> Merci d'en saisir une autre !";
                }
            }
            else
            {
                $erreur = "Votre Adresse e-mail n'est pas valide !";
            }
        }
        else
        {
            $erreur = "Merci de saisir des Adresses e-mail identiques !";
        }
    }

    if(isset($_POST['newmdp']) AND !empty($_POST['newmdp']) AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2']))
    {
        $mdp = $_POST['newmdp'];
        $mdp2 = $_POST['newmdp2'];

        $hashedpass = password_hash($mdp, PASSWORD_DEFAULT);

        if($mdp == $mdp2)
        {
            $updateemail = $bdd->prepare("UPDATE membres SET motdepasse = ? WHERE id_membres = ?");
            $updateemail->execute(array($hashedpass, $_SESSION['id_membres']));
            $changed = true;
        }
        else
        {
            $erreur = "Merci de saisir des Mots de passe correspondants !";
        }
    }

    if($changed == true  or (isset($_POST['editprofil']) AND $_POST['editprofil'] != ''))
    {
        header("Location: profil.php?id_membres=".$_SESSION['id_membres']);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Votre profil</title>
</head>
<body>

    <div>
        <h2 align="center">Edition de mon profil</h2>

        <div align="center">
            <form method="POST" action="" enctype="multipart/form-data">

                <br />

                <table>
                    <tr>
                        <td align="center">
                            <label for="avatar">Image de profil :</label>
                        </td>
                        <td>
                            <input type="file" name="avatar" id="avatar" />
                        </td>
                    </tr>
                </table>

                <br />

                <table>
                    <tr>
                        <td align="center">
                            <label for="pseudo">Pseudo :</label>
                        </td>
                        <td>
                            <input type="text" name="newpseudo" id="pseudo" placeholder="Pseudo" value="<?= $user['pseudo'] ?>" />
                        </td>
                    </tr>
                </table>

                <br />

                <div>
                    <tr>
                        <td align="center">
                            <label for="nom">Nom :</label>
                        </td>
                        <td>
                            <input type="text" name="newnom" id="nom" placeholder="Nom" value="<?= $user['nom'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <label for="prenom">Prénom :</label>
                        </td>
                        <td>
                            <input type="text" name="newprenom" id="prenom" placeholder="Prénom" value="<?= $user['prenom'] ?>" />
                        </td>
                    </tr>
                </div>

                <br />

                <table>
                    <tr>
                        <td align="center">
                            <label for="email">E-mail :</label>
                        </td>
                        <td>
                            <input type="email" name="newemail" id="email" placeholder="Adresse e-mail*" value="<?= $user['email'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <label for="email2">Confirmation de l'e-mail :</label>
                        </td>
                        <td>
                            <input type="email" name="newemail2" id="email2" placeholder="Adresse e-mail de confirmation*" />
                        </td>
                    </tr>

                    <br />

                    <tr>
                        <td align="center">
                            <label for="mdp">Mot de passe :</label>
                        </td>
                        <td>
                            <input type="password" name="newmdp" id="mdp" placeholder="Mot de passe*" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <label for="mdp2">Confirmation du mot de passe :</label>
                        </td>
                        <td>
                            <input type="password" name="newmdp2" id="mdp2" placeholder="Mot de passe de confirmation*" />
                        </td>
                    </tr>
                </table>

                <br />

                <table align="center">
                    <tr>
                        <td></td>
                        <td align="center">
                            <br />
                            <input class="btn btn-success btn-sm" type="submit" name="editprofil" value="MISE À JOUR DU PROFIL" />
                        </td>
                        <td></td>
                        <td></td>
                        <td align="center">
                            <br />
                            <a href="<?= "/profil.php?id_membres=".$_SESSION['id_membres'] ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">RETOUR</a>
                        </td> 
                    </tr>
                </table>

            </form>

            <br />

            <?php
                if(isset($erreur))
                {
                    echo '<font color="red">'.$erreur.'</font>';
                }
            ?>

        </div>

    </div>

</body>
</html>

<?php

}
else
{
    header("Location: connexion.php");
}

?>
