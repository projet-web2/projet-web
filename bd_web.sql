-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour espace_membre
CREATE DATABASE IF NOT EXISTS `espace_membre` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `espace_membre`;

-- Listage de la structure de la table espace_membre. articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(250) DEFAULT NULL,
  `contenu` text,
  `prix` float DEFAULT NULL,
  `prix_occasion` float DEFAULT NULL,
  `categories` set('VOITURES','MULTIMEDIAS','VETEMENTS','OCCASIONS') DEFAULT NULL,
  `pseudo` varchar(250) DEFAULT NULL,
  `id_vendeur` int(11) DEFAULT NULL,
  `date_time_publi` datetime DEFAULT NULL,
  `date_time_edition` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.articles : ~10 rows (environ)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `titre`, `contenu`, `prix`, `prix_occasion`, `categories`, `pseudo`, `id_vendeur`, `date_time_publi`, `date_time_edition`) VALUES
	(2, 'Bioshock', 'FPS', 20, 15, '', 'Curseur', 1, '2020-04-21 18:26:25', NULL),
	(5, 'TitanFall', 'FPS avec des Titans', 30, 30, 'MULTIMEDIAS', 'Curseur', 1, '2020-04-21 20:26:17', NULL),
	(6, 'WOW', 'MMORPG ', 60, 60, 'MULTIMEDIAS', 'Curseur', 1, '2020-04-22 14:54:54', NULL),
	(8, 'Bugatti XV2', 'Super voiture de sport ', 250000, 250000, 'VOITURES', 'Curseur', 1, '2020-04-22 18:54:28', NULL),
	(9, 'Poussette', 'Encore en trÃ¨s bon Ã©tat. Utiliser qu\'une seule fois.', 65, 65, 'OCCASIONS', 'Curseur', 1, '2020-04-22 18:56:49', '2020-04-22 21:38:25'),
	(10, 'Chapeau de Laiton', 'Chapeau beau', 10, 10, 'VETEMENTS', 'Lhaurel', 2, '2020-04-22 18:57:54', NULL),
	(12, 'Manette PS4', 'Je me sÃ©pare de ma manette de PS4, elle ne me sert plus Ã  rien, j\'en ai beaucoup trop.', 25, 20, 'OCCASIONS', 'Curseur', 1, '2020-04-22 21:40:08', NULL),
	(15, 'Banane Divine', 'Elle est magique je vous jure, elle se transforme en voiture !', 1000, 1000, 'VOITURES', 'Lhaurel', 2, '2020-04-22 22:19:56', '2020-04-22 22:25:46'),
	(16, 'Moto Cross', 'Pour faire du Trial y a rien de mieux, elle est bien rodÃ©e !', 1250, 1100, 'VOITURES', 'Lhaurel', 2, '2020-04-22 22:26:49', NULL),
	(23, 'Chapeau', 'Chapeau', 15, 15, 'VETEMENTS', 'Curseur', 1, '2020-04-23 03:14:58', '2020-04-23 03:16:34');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. discu
CREATE TABLE IF NOT EXISTS `discu` (
  `id_discu` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id_discu`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.discu : ~29 rows (environ)
/*!40000 ALTER TABLE `discu` DISABLE KEYS */;
INSERT INTO `discu` (`id_discu`, `pseudo`, `user_id`, `message`) VALUES
	(15, 'Curseur', NULL, 'Salut'),
	(16, 'Sixte', NULL, 'Hello'),
	(18, 'Curseur', NULL, 'Salut, comment Ã§a va ?'),
	(19, 'Sixte', NULL, 'Coucou ! Je vais bien et toi?'),
	(20, 'Sixte', NULL, 'Tu fais quoi ici?'),
	(21, 'Curseur', NULL, 'Je vais bien, je te remercie.\r\nJe suis ici car je cherche une rÃ©ponse Ã  ma question'),
	(22, 'Sixte', NULL, 'C\'est quoi ta question? Peut Ãªtre que les personnes inscrites sur le site (et qui ont accÃ¨s au chan de discussion) pourront te rÃ©pondre !'),
	(23, 'Curseur', NULL, 'Je voudrais savoir comment mettre en place un systÃ¨me de rÃ©cupÃ©ration de mot de passe.'),
	(24, 'Curseur', NULL, 'Hello tout le monde !'),
	(25, 'Viktor', NULL, 'Salut !'),
	(26, 'Curseur', 1, 'Test 1 2 1 2'),
	(28, 'Curseur', 1, 'Salut !'),
	(29, 'Guest', NULL, 'Salut'),
	(30, 'Lhaurel', 2, 'Yo !'),
	(31, 'Guest', NULL, 'Comment tu vas?'),
	(32, 'Lhaurel', 2, 'Je vais bien'),
	(33, 'Lhaurel', 2, 'Et toi?'),
	(35, 'Lhaurel', 2, 'Allo?'),
	(37, 'Guest', NULL, 'Ouais pardon !'),
	(39, 'Lhaurel', 2, 'Ok cool'),
	(41, 'Lhaurel', 2, 'Tu fais quoi ce soir?'),
	(42, 'Guest', NULL, 'Rien'),
	(43, 'Lhaurel', 2, 'Mince'),
	(44, 'Guest', NULL, ':\'('),
	(45, 'Curseur', 1, 'Salut on est le 21/04/2020 !'),
	(46, 'Lhaurel', 2, 'Salut'),
	(47, 'Lhaurel', 2, 'Salut'),
	(48, 'Curseur', 1, 'Salut'),
	(49, 'Curseur', 1, 'Salut');
/*!40000 ALTER TABLE `discu` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. membres
CREATE TABLE IF NOT EXISTS `membres` (
  `id_membres` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(250) DEFAULT NULL,
  `nom` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `motdepasse` text,
  `avatar` varchar(250) DEFAULT NULL,
  `confirmkey` varchar(250) DEFAULT NULL,
  `confirme` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_membres`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.membres : ~6 rows (environ)
/*!40000 ALTER TABLE `membres` DISABLE KEYS */;
INSERT INTO `membres` (`id_membres`, `pseudo`, `nom`, `prenom`, `email`, `motdepasse`, `avatar`, `confirmkey`, `confirme`) VALUES
	(1, 'Curseur', 'Malo', 'Valentin', 'kalki-wow@live.fr', '$2y$10$BYiH9hMWmVzg4BRrYqjLkOwIWj84D/Twu5yJ76Ni3UFaO2Hxj9YmS', '1.jpg', NULL, NULL),
	(2, 'Lhaurel', 'Malo', 'Baptiste', 'sixter-wow@live.fr', '$2y$10$XFe/PjpXlZl.RYZKnnR8xufduKWblDjSVAqxFhjdobmLmDHpTU8UC', '2.png', NULL, NULL),
	(10, 'Cucu', 'MRTY', 'John', 'kalkivalentin117@gmail.com', '$2y$10$1BmXuZzZdlcOXS1N.R8/WO8tbHZJCw8gPXVNtpL3RzQ9EmtJ1axIi', 'default.png', NULL, NULL),
	(11, 'Test', 'Test', 'Test', 'test@gmail.com', '$2y$10$3wDXK6kjexNnmExj0KeKw.yb4mqhnYMl6JT5rURa2CuuX8updZH3C', '11.jpg', NULL, NULL),
	(12, 'Sixtte', 'Berthier', 'ValÃ©rie', 'sixte-wow@live.fr', '$2y$10$t6tX.jYCR8GqMrgm9uCHJOHymoGMtnRhStaInIWcZ8iJKeLE0gadm', '12.jpg', NULL, NULL),
	(13, 'Kalky', 'Hunter', 'Shang', 'LhaurelXIII-wow@outlook.com', '$2y$10$n/Bf.eDqY9TaeYjMTLweX.LJtnTYwrde/G7..Y.lQsoQ.k3hh0ZQa', 'default.png', '92274770605006', 1),
	(14, 'Cucu', 'Cucu', 'Lapraline', 'cucu@gmail.com', '$2y$10$5b7mT1biG2lEOIyCHW7S9..NWvnPB3PVLvf.IdfpXNYgkgwL5y4DC', 'default.png', '10400649994415', 1);
/*!40000 ALTER TABLE `membres` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. message
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediteur` int(11) DEFAULT NULL,
  `id_destinataire` int(11) DEFAULT NULL,
  `sujet` varchar(250) DEFAULT NULL,
  `message` text,
  `date_time` datetime DEFAULT NULL,
  `lu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.message : ~8 rows (environ)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`id`, `id_expediteur`, `id_destinataire`, `sujet`, `message`, `date_time`, `lu`) VALUES
	(6, 1, 2, 'Bonjour le nouveau', 'Salut !', NULL, 1),
	(12, 1, 2, 'Objet de test', 'Je te passe le Bonjour !\r\nSale humain !', NULL, 1),
	(15, 1, 2, 'RE : RE : Objet de test', 'Cool !', NULL, 1),
	(16, 1, 2, 'Test', 'Salut !', NULL, 1),
	(19, 1, 2, 'RE : Date de test', 'Ã§a marche la Date maintenant Ouiiiii', '2020-04-21 20:49:56', 1),
	(20, 1, 2, 'Salut', 'Re-test', '2020-04-22 15:40:40', 1),
	(21, 1, 2, 'Test', 'Test', '2020-04-23 17:31:51', 1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

-- Listage de la structure de la table espace_membre. recuperation
CREATE TABLE IF NOT EXISTS `recuperation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `confirme` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table espace_membre.recuperation : ~0 rows (environ)
/*!40000 ALTER TABLE `recuperation` DISABLE KEYS */;
/*!40000 ALTER TABLE `recuperation` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
