<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

if(isset($_GET['section']))
{
    $section = htmlspecialchars($_GET['section']);
}
else
{
    $section = "";
}

if(isset($_POST['recup_submit'], $_POST['recup_email']))
{
    if(!empty($_POST['recup_email']))
    {
        $recup_email = htmlspecialchars($_POST['recup_email']);
        if(filter_var($recup_email, FILTER_VALIDATE_EMAIL))
        {
            $emailexist = $bdd->prepare("SELECT id_membres, pseudo FROM membres WHERE email = ?");
            $emailexist->execute(array($recup_email));
            $emailexist_count = $emailexist->rowCount();
            if($emailexist_count == 1)
            {
                $pseudo = $emailexist->fetch();
                $pseudo = $pseudo['pseudo'];

                $_SESSION['recup_email'] = $recup_email;
                $recup_code = "";
                for($i = 0; $i < 8; $i++)
                {
                    $recup_code .= mt_rand(0, 9);
                }

                $email_recupexist = $bdd->prepare("SELECT id FROM recuperation WHERE email = ?");
                $email_recupexist->execute(array($recup_email));
                $email_recupexist = $email_recupexist->rowCount();

                if($email_recupexist == 1)
                {
                    $recup_insert = $bdd->prepare("UPDATE recuperation SET code = ? WHERE email = ?");
                    $recup_insert->execute(array($recup_code, $recup_email));
                }
                else
                {
                    $recup_insert = $bdd->prepare("INSERT INTO recuperation(email, code) VALUES (?, ?)");
                    $recup_insert->execute(array($recup_email, $recup_code));
                }

                $header = "MIME-Version: 1.0\r\n";
                $header .= 'From: "TyuiopCase.com"<support@tyuiopcase.com>'."\n";
                $header .= 'Content-Type: text/html; charset="UTF-8"'."\n";
                $header .= 'Content-Transfer-Encoding: 8bit';

                $message = '
                <html>
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Récupération de mot de passe - TyuiopCase.com</title>
                </head>
                <body>
                    <div align="center">
                        <table>
                            <tr>
                                <td>
                                    <br />
                                    <div align="center">
                                        Bonjour <b>'.$pseudo.'</b> !
                                    </div>
                                    <br /><br />
                                    Voici votre code de récupération : <b>'.$recup_code.'</b>
                                    <br /><br />
                                    A bientôt sur <a href="http://projet-web.test/index.php">TyuiopCase.com</a> !
                                    <br />
                                </td>
                            </tr>
                            <br /><br />
                            <tr>
                                <td align="center">
                                    Ceci est un e-mail automatique, merci de ne pas y répondre ! En vous remerciant.
                                </td>
                            </tr>
                                J\'ai envoyé cet e-mail avec PHP !
                                <br />
                        </table>
                    </div>
                </body>
                </html>
                ';

                mail($recup_email, "Récupération de Mot de passe - TyuiopCase.com", $message, $header);
                header("Location: recuperation.php?section=code");

            }
            else
            {
                $erreur = "Cette Adresse e-mail n'existe pas, merci de vous enregistrez !";
            }
        }
        else
        {
            $erreur = "Adresse e-mail invalide";
        }
    }
    else
    {
        $erreur = "Veuillez entrer votre Adresse e-mail";
    }
}

if(isset($_POST['code_submit'], $_POST['code_verif']))
{
    if(!empty($_POST['code_verif']))
    {
        $code_verif = htmlspecialchars($_POST['code_verif']);
        $code_req = $bdd->prepare("SELECT * FROM recuperation WHERE email = ? AND code = ?");
        $code_req->execute(array($_SESSION['recup_email'], $code_verif));
        $code_req = $code_req->rowCount();
        if($code_req == 1)
        {
            $update_req = $bdd->prepare("UPDATE recuperation SET confirme = 1 WHERE email = ?");
            $update_req->execute(array($_SESSION['recup_email']));
            header("Location: recuperation.php?section=changemdp");
        }
        else
        {
            $erreur = "Code invalide.";
        }
    }
    else
    {
        $erreur = "Veuillez saisir votre code de vérification reçu par e-mail !";
    }
}

if(isset($_POST['change_submit']))
{
    if(isset($_POST['change_mdp'], $_POST['change_mdp_confirm']))
    {
        $verif_confirme = $bdd->prepare("SELECT confirme FROM recuperation WHERE email = ?");
        $verif_confirme->execute(array($_SESSION['recup_email']));
        $verif_confirme = $verif_confirme->fetch();
        $verif_confirme = $verif_confirme['confirme'];
        if($verif_confirme == 1)
        {
            $mdp = htmlspecialchars($_POST['change_mdp']);
            $mdpc = htmlspecialchars($_POST['change_mdp_confirm']);
            $hashedpass = password_hash($mdp, PASSWORD_DEFAULT);
            if(!empty($mdp) AND !empty($mdpc))
            {
                if($mdp == $mdpc)
                {
                    $ins_mdp = $bdd->prepare("UPDATE membres SET motdepasse = ? WHERE email = ?");
                    $ins_mdp->execute(array($hashedpass, $_SESSION['recup_email']));
                    $del_req = $bdd->prepare("DELETE FROM recuperation WHERE email = ?");
                    $del_req->execute(array($_SESSION['recup_email']));
                    $msg = "Votre de Mot de passe a bien été modifié !";
                    header("Location: connexion.php");
                }
                else
                {
                    $erreur = "Merci de saisir des Mots de passe correspondants !";
                }
            }
            else
            {
                $erreur = "Veuillez remplir tous les champs !";
            }
        }
        else
        {
            $erreur = "Impossible de ré-initialiser votre Mot de passe sans vérification de votre Adresse e-mail !
            <br />
            Valider votre Adresse e-mail avec le code de vérification envoyé par e-mail.";
        }
    }
    else
    {
        $erreur = "Veuillez remplir tous les champs !";
    }
}

require_once('recuperation_view.php');

?>