<nav class="navbar navbar-dark bg-dark sticky-top">
    <h2><a class="navbar-brand" href="<?= "../monprofil.php?id_membres=".$_SESSION['id_membres'] ?>">TyuiopCase Menu</a></h2>
    <div class="nav-item">
        <h5><a class="nav-link" href="inscription.php">S'inscrire</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="connexion.php">Se connecter</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="deconnexion.php">Se déconnecter</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="<?= "../login/profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="../annonce/annonces.php">Mes Annonces</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="../annonce/addannonces.php">Ajouter des Annonces</a></h5>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
            <br />
            <nav class="navbar navbar-dark bg-primary"></nav>
            <br />
            <div align="center">
                <h4 style="color: deepskyblue">Les Catégories</h4>
            </div>
            <br />
            <ul class="topLevelMenu">
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/voiture.php" style="color: white">VOITURES</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/vetement.php" style="color: white">VÊTEMENTS</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/occasion.php" style="color: orange">OCCASIONS</a></h5>
                </li>
            </ul>
        </nav>
    </div>
</nav>
<br />

<?php

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

if(isset($_POST['forminscription']))
{
    $pseudo = htmlspecialchars($_POST['pseudo']);
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $email = htmlspecialchars($_POST['email']);
    $email2 = htmlspecialchars($_POST['email2']);
    $mdp = ($_POST['mdp']);
    $mdp2 = ($_POST['mdp2']);

    $hashedpass = password_hash($mdp, PASSWORD_DEFAULT);
    
    if(!empty($_POST['pseudo']) AND !empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['email']) AND !empty($_POST['email2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2']))
    {
        $pseudolength = strlen($pseudo);
        if($pseudolength <= 250)
        {
            $nomlength = strlen($nom);
            if($nomlength <= 250)
            {
                $prenomlength = strlen($prenom);
                if($prenomlength <= 250)
                {
                    if($email == $email2)
                    {
                        if(filter_var($email, FILTER_VALIDATE_EMAIL))
                        {
                            $reqemail = $bdd->prepare("SELECT * FROM membres WHERE email = ?");
                            $reqemail->execute(array($email));
                            $emailexist = $reqemail->rowCount();
                            if($emailexist == 0)
                            {
                                if($mdp == $mdp2)
                                {
                                    $longueur_key = 15;
                                    $key = "";
                                    for($i = 1; $i < $longueur_key; $i++)
                                    {
                                        $key .= mt_rand(0, 9);    
                                    }

                                    $insertmbr = $bdd->prepare("INSERT INTO membres(pseudo, nom, prenom, email, motdepasse, avatar, confirmkey) VALUES(?, ?, ?, ?, ?, ?, ?)");
                                    $insertmbr->execute(array($pseudo, $nom, $prenom, $email, $hashedpass, "default.png", $key));

                                    $header = "MIME-Version: 1.0\r\n";
                                    $header .= 'From: "TyuiopCase.com"<support@tyuiopcase.com>'."\n";
                                    $header .= 'Content-Type: text/html; charset = "utf-8"'."\n";
                                    $header .= 'Content-Transfer-Encoding: 8bit';

                                    $message = '
                                        <html>
                                            <body>
                                                <div align="center">
                                                    <a href="http://projet-web.test/login/confirmation.php?email='.urlencode($email).'&key='.$key.'">Confirmez votre compte !</a>
                                                </div>
                                            </body>
                                        </html>
                                    ';

                                    mail($email, "Bonjour, ceci est un e-mail de confirmation de la création de votre compte sur TyuiopCase.com", $message, $header);
                                    
                                    $erreur = "Votre compte a bien été enregistré ! <a href=\"connexion.php\">Connexion</a>";
                                    $erreur1 = "Merci de regarder vos e-mails pour confirmer votre compte.";
                                }
                                else
                                {
                                    $erreur = "Merci de saisir des Mots de passe correspondants !";
                                }
                            }
                            else
                            {
                                $erreur = "L'Adresse e-mail est déjà utilisée ! <br/> Merci d'en saisir une autre !";
                            }
                        }
                        else
                        {
                            $erreur = "Votre Adresse e-mail n'est pas valide !";
                        }
                    }
                    else
                    {
                        $erreur = "Merci de saisir des Adresses e-mail identiques !";
                    }
                }
                else
                {
                    $erreur = "Votre Prénom est beaucoup trop long !";
                }
            }
            else
            {
                $erreur = "Votre Nom de famille est beaucoup trop long !";
            }
        }
        else
        {
            $erreur = "Votre Pseudo est beaucoup trop long !";
        }
    }
    else
    {
        $erreur = "Tous les champs doivent être complétés !";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="page accueil.html">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <title>Inscription</title>
</head>
<body>
    
    <div align="center">
        <h2>Inscription</h2>

        <br /><br />

        <form method="POST" action="">

            <table>
                <tr>
                    <td align="right">
                        <label for="pseudo">Pseudo :</label>
                    </td>
                    <td>
                        <input type="text" placeholder="Pseudo*" id="pseudo" name="pseudo" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>" />
                        <span>Ex : FièvrePoireau</span>
                    </td>
                </tr>
            </table>
            <div>
                <tr>
                    <td align="right">
                        <label for="nom">Nom :</label>
                    </td>
                    <td>
                        <input type="text" placeholder="Nom de famille* Ex : Fleur" id="nom" name="nom" value="<?php if(isset($nom)) { echo $nom; } ?>" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <label for="prenom">Prénom :</label>
                    </td>
                    <td>
                        <input type="text" placeholder="Prenom* Ex : Elena" id="prenom" name="prenom" value="<?php if(isset($prenom)) { echo $prenom; } ?>" />
                    </td>
                </tr>
            </div>
            <table>
                <tr>
                    <td align="right">
                        <label for="email">E-mail :</label>
                    </td>
                    <td>
                        <input type="email" placeholder="Adresse e-mail*" id="email" name="email" value="<?php if(isset($email)) { echo $email; } ?>" />
                        <span>Ex : fleur.elena@tyuiopcase.com</span>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <label for="email2">Confirmation de l'e-mail :</label>
                    </td>
                    <td>
                        <input type="email" placeholder="Adresse e-mail de confirmation*" id="email2" name="email2" value="<?php if(isset($email2)) { echo $email2; } ?>" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <label for="mdp">Mot de passe :</label>
                    </td>
                    <td>
                        <input type="password" placeholder="Mot de passe*" id="mdp" name="mdp" />
                        <span>Ex : Gig4hardPassw0rd!117</span>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <label for="mdp2">Confirmation du mot de passe :</label>
                    </td>
                    <td>
                        <input type="password" placeholder="Mot de passe de confirmation*" id="mdp2" name="mdp2" />
                    </td>
                </tr>
            </table>
            <table align="center">
                <tr>
                    <td></td>
                    <td>
                        <br />
                        <input class="btn btn-primary btn-sm" type="submit" name="forminscription" value="S'ENREGISTRER" />
                    </td>
                </tr>
            </table>
            <br>
            <div>
                <p><b>Vous êtes déjà inscrit, connectez-vous !</b></p>
                <a class="btn btn-primary btn-sm" href="connexion.php">Connexion</a>
            </div>
            
        </form>

        <br />

        <?php
            if(isset($erreur))
            {
                echo '<font color="red">'.$erreur.'</font>';
            }
        ?>
        <br />
        <?php
            if(isset($erreur1))
            {
                echo '<font color="green">'.$erreur1.'</font>';
            }
        ?>

    </div>

</body>
</html>