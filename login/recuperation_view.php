<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Récupération de Mot de passe</title>
    <link rel="canonical" href="http://projet-web.test/login/connexion.php" />
    <meta name="description" content="Vous avez oublié votre mot de passe ? C'est par ici !" />
</head>
<body>

    <div>
        <div>
            <div>
                <div>
                    <div>
                        <ol>
                            <ul>
                                <span>Vous êtes sur la page de récupération de votre mot de passe : </span>
                                <br /><br />
                                <a href="recuperation.php">Récupération de Mot de passe</a>
                            </ul>
                            <br />
                            <ul>
                                <span>Pour revenir sur la page de connexion : </span><a href="connexion.php">Connexion</a>
                            </ul>
                            <ul>
                                <span>Pour rejoindre la page d'inscription : </span><a href="inscription.php">Inscription</a>
                            </ul>
                        </ol>
                        <span></span>
                    </div>
                    <article>
                        <h3>Récupération de Mot de passe</h3>
                        <?php
                            if($section == 'code')
                            {
                        ?>
                                Récupération de Mot de passe.
                                <br />
                                Un code de vérification vous a été envoyé par e-mail : <?= $_SESSION['recup_email'] ?>
                                <br /><br />
                                <form method="POST">
                                    <input type="text" placeholder="Code de vérification" name="code_verif" />
                                    <br /><br />
                                    <input type="submit" value="Valider" name="code_submit" />
                                </form>
                        <?php
                            }
                            elseif($section == "changemdp")
                            {
                        ?>
                                Nouveau Mot de passe pour <?= $_SESSION['recup_email'] ?>
                                <br /><br />
                                <form method="POST">
                                    <input type="password" placeholder="Nouveau Mot de passe" name="change_mdp" />
                                    <br /><br />
                                    <input type="password" placeholder="Confirmation du Mot de passe" name="change_mdp_confirm" />
                                    <br /><br />
                                    <input type="submit" value="Valider" name="change_submit" />
                                </form>
                        <?php
                            }
                            else
                            {
                        ?>
                                <h4>Pour récupérer votre Mot de passe, merci de saisir votre Adresse e-mail ci-dessous :</h4>
                                <form method="POST">
                                    <input type="email" placeholder="Votre Adresse e-mail" name="recup_email" />
                                    <br /><br />
                                    <input type="submit" value="Valider" name="recup_submit" />
                                </form>
                        <?php
                            }
                        ?>
                        <br />
                        <?php
                            if(isset($erreur))
                            {
                                echo '<font color="red">'.$erreur.'</font>';
                            }
                        ?>
                    </article>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>