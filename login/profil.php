<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('../cookieconnect.php');

if(isset($_GET['id_membres']) AND $_GET['id_membres'] > 0)
{
    $getid = intval($_GET['id_membres']);
    $requser = $bdd->prepare("SELECT * FROM membres WHERE id_membres = ?");
    $requser->execute(array($getid));
    $userinfo = $requser->fetch();

?>

<nav class="navbar navbar-dark bg-dark sticky-top">
    <h2><a class="navbar-brand" href="<?= "../monprofil.php?id_membres=".$_SESSION['id_membres'] ?>">TyuiopCase Menu</a></h2>
    <div class="nav-item">
        <h5><a class="nav-link" href="../annonce/annonces.php">Mes Annonces</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="nav-link" href="../annonce/addannonces.php">Ajouter des Annonces</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="editionprofil.php">Editer profil</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="invisible" href="<?= "profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="nav-link" href="deconnexion.php">Se Déconnecter</a></h5>
    </div>
    <div class="nav-item" style="color: white">
        <h5><?= $userinfo['pseudo'] ?></h5>
    </div>
    <div class="nav-item">
        <img src="membres/avatars/<?= $userinfo['avatar']; ?>" width="50px" />
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
            <br />
            <nav class="navbar navbar-dark bg-primary"></nav>
            <br />
            <div align="center">
                <h4 style="color: deepskyblue">Les Catégories</h4>
            </div>
            <br />
            <ul class="topLevelMenu">
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/voiture.php" style="color: white">VOITURES</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/vetement.php" style="color: white">VÊTEMENTS</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="../annonce/occasion.php" style="color: orange">OCCASIONS</a></h5>
                </li>
            </ul>
        </nav>
    </div>
</nav>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="logo.png" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="page accueil.html">

    <title>Votre profil</title>
</head>
<body>

<div><br></div>
<section class="container mt-4 mb-4">
    <div class="container">
		<div class="d-flex flex-row border rounded">
	  		<div class="p-0 w-55">
                <img class="imgcadre" src="membres/avatars/<?= $userinfo['avatar']; ?>" width="325" class="img-thumbnail border-0"/>
	  		</div>
	  		<div class="pl-3 pt-2 pr-2 pb-2 w-75 border-left">
	  			<h4 class="text-primary"><?= $userinfo['pseudo'] ?></h4>
	  			<h5 class="text-info"><?= $userinfo['nom'] ?> <?= $userinfo['prenom'] ?></h5>
                <h5 class="text-info"><?= $userinfo['email'] ?></h5>
	  			<ul class="m-0 float-left" style="list-style: none; margin:0; padding: 0">
	  				<li><a href="../messagerie/reception.php" class="btn btn-info btn-sm"> <i class="fa fa-envelope"></i> Ma Messagerie</a></li>
	  				<li class="invisible"><i class="fab fa-twitter-square"></i> Twitter</li>
	  			</ul>
                <div style="padding-top: 6%">
					<p class="text-right m-0"><a href="editionprofil.php" class="btn btn-primary"><i class="far fa-edit"></i> Modifier mon profil</a></p>
                </div>
			</div>
		</div>
	</div>    
</section>

<div align="center">
    <a  class="btn btn-primary btn-sm" href="<?= "../monprofil.php?id_membres=".$_SESSION['id_membres'] ?>"><i class="fa fa-cart-arrow-down"></i> Accueil du site</a>

    <br /><br />


    <?php
        if(!empty($userinfo['avatar']))
        {
    ?>
            
    <?php
        }
    ?>

    <br /><br />

    <?php
        if(isset($_SESSION['id_membres']) AND $userinfo['id_membres'] == $_SESSION['id_membres'])
        {
    ?>
            
    <?php
        }
    ?>

    </div>

</body>
</html>

<?php

}

?>
