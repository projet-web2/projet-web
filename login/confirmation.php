<?php

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

if(isset($_GET['email'], $_GET['key']) AND !empty($_GET['email']) AND !empty($_GET['key']))
{
    $email = htmlspecialchars(urldecode($_GET['email']));
    $key = htmlspecialchars($_GET['key']);

    $requser = $bdd->prepare("SELECT * FROM membres WHERE email = ? AND confirmkey = ?");
    $requser->execute(array($email, $key));
    $userexist = $requser->rowCount();

    if($userexist == 1)
    {
        $user = $requser->fetch();
        if($user['confirme'] == 0)
        {
            $updateuser = $bdd->prepare("UPDATE membres SET confirme = 1 WHERE email = ? AND confirmkey = ?");
            $updateuser->execute(array($email, $key));
            echo "Votre compte a bien été confirmé !";
            ?><br /><br /><?php
            echo '<a href="connexion.php">Connectez-vous</a>';
        }
        else
        {
            echo "Votre compte a déjà été confirmé !";
            ?><br /><br /><?php
            echo '<a href="connexion.php">Connectez-vous</a>';
        }
    }
    else
    {
        echo "La clé que vous avez validé ne correspond à votre utilisateur !";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation compte</title>
</head>
<body>
    
</body>
</html>