<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('../cookieconnect.php');

$all_msg = $bdd->query("SELECT * FROM discu ORDER BY id_discu DESC");
while ($msg = $all_msg->fetch()) {
?>
    <?php 
        if ($msg['user_id'] != null) : 
    ?>
            <a href="../login/profil.php?id_membres=<?= $msg['user_id'] ?>"><b><?= $msg['pseudo'] ?></b></a>
    <?php 
        else : 
    ?>
            <b><?= $msg['pseudo'] ?></b>
    <?php 
        endif; 
    ?>
    <?= nl2br($msg['message']); ?>
    <br>
<?php
};
?>