<?php

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('../cookieconnect.php');

if (isset($_POST['message']) and !empty($_POST['message'])) {
    $message = htmlspecialchars($_POST['message']);
    
    /* les lignes 12 et 13 \/
    $userid = (isset($_SESSION['id_membres']) && !empty($_SESSION['id_membres'])) ? $_SESSION['id_membres'] : null;
    $pseudo = (isset($_SESSION['pseudo']) && !empty($_SESSION['pseudo'])) ? $_SESSION['pseudo'] : 'Guest';
    sont = aux lignes 15 à 21  \/ */
    if (isset($_SESSION['id_membres']) && !empty($_SESSION['id_membres'])) {
        $userid = $_SESSION['id_membres'];
        $pseudo = $_SESSION['pseudo'];
    } else {
        $userid = null;
        $pseudo = 'Guest';
    }

    $insertmsg = $bdd->prepare("INSERT INTO discu(pseudo, message, user_id) VALUES (?, ?, ?)");
    $insertmsg->execute(array($pseudo, $message, $userid));
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Channel de discussion</title>
</head>

<body>
    <form method="POST" action="">
        <?php 
            if (isset($_SESSION['id_membres']) and !empty($_SESSION['id_membres'])) : 
        ?>
                <!-- <input type="hidden" name="pseudo" placeholder="Votre Pseudo" value="<?= $_SESSION['pseudo'] ?>" /> -->
                <b><?= $_SESSION['pseudo'] ?></b>
        <?php 
            else : 
        ?>
                <!-- <input type="hidden" name="pseudo" placeholder="Votre Pseudo" value="Guest"> -->
                <b>Guest</b>
        <?php 
            endif; 
        ?>
        <br /><br />
        <textarea type="text" name="message" placeholder="Saisir votre message"></textarea>
        <br /><br />
        <input type="submit" value="Envoyer !" />
        <br /><br />
    </form>

    <div id="message">
        <?php include_once('loadmessages.php'); ?>
    </div>

    <script>
        setInterval('load_messages()', 1500);

        function load_messages() {
            $('#message').load('loadmessages.php');
        }
    </script>

    <br /><br />
    <a href=<?= "../monprofil.php?id_membres=".$_SESSION['id_membres'] ?> >Retour au Site</a>

</body>

</html>