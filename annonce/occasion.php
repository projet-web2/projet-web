<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('cookieconnect.php');

$articles = $bdd->prepare("SELECT * FROM articles WHERE categories = ? ORDER BY date_time_publi DESC");
$articles->execute(array('OCCASIONS'));

if(isset($_GET['r']) AND !empty($_GET['r']))
{
    $r = htmlspecialchars($_GET['r']);

    $r_array = explode(' ', $r);

    $articles = $bdd->query("SELECT * FROM articles WHERE titre LIKE '%".$r."%' AND categories = 'OCCASIONS' ORDER BY id DESC");
    if($articles->rowCount() == 0)
    {
        $articles = $bdd->query("SELECT * FROM articles WHERE CONCAT(titre, contenu) LIKE '%".$r."%' AND categories = 'OCCASIONS' ORDER BY id DESC");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="style.css" rel="stylesheet" >
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les Annonces Occasions</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark sticky-top">
      <h2><a class="navbar-brand" href="<?= "/monprofil.php?id_membres=".$_SESSION['id_membres'] ?>">TyuiopCase Menu</a></h2>

      <div class="nav-item">
        <h5><a class="nav-link" href="annonces.php">Mes Annonces</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="nav-link" href="addannonces.php">Ajouter des Annonces</a></h5>
      </div>
      <div class="nav-item">
      <h5><a class="nav-link" href="<?= "/profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
      </div>
      <div class="nav-item">
        <h5><a class="nav-link" href="deconnexion.php">Se déconnecter</a></h5>
      </div>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
          <br />
          <nav class="navbar navbar-dark bg-primary"></nav>
          <br />
          <div align="center">
            <h4 style="color: deepskyblue">Les Catégories</h4>
          </div>
          <br />
          <ul class="topLevelMenu">
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
            </li>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <li class="menuFooter">
              <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
            </li>
          </ul>
        </nav>
      </div>
    </nav>

    <br /><br />
    <div align="center">
    <form method="GET">
        <input type="search" name="r" placeholder="Recherche" />
        <input class="btn btn-success btn-sm" type="submit" value="Valider" />
    </form>
    <br />
    <?php if($articles->rowCount() > 0)
        {
    ?>
      
            <?php
                while($a = $articles->fetch())
                {
            ?>
                    
                        <b><?= $a['titre'] ?></b>
                        <br />
                        Prix : <?=$a['prix']?> € --- Prix occasion : <?= $a['prix_occasion'] ?> €
                        <br><br>
                        <img src="miniatures/<?= $a['id'] ?>.jpg" width="200" />
                        <br><br>
                        Description : <?= $a['contenu'] ?>
                        <br>                        
                        <br>
                        Annonce publié le : <?= $a['date_time_publi'] ?> par 
                        <?php 
                            if ($a['id_vendeur'] != null) : 
                        ?>
                                <a href="profil.php?id_membres=<?= $a['id_vendeur'] ?>"><b><?= $a['pseudo'] ?></b></a>
                        <?php 
                            else : 
                        ?>
                                <b><?= $a['pseudo'] ?></b>
                        <?php 
                            endif; 
                        ?>.
                        <br /><br />
                        <a class="btn btn-info btn-sm" href="article_occasion.php?id=<?= $a['id'] ?>">Voir l'article</a>
                        <br />
                        -----------------------------
                        <br /><br />
                    
            <?php
                }
            ?>
        
    <?php
        }
        else
        {
    ?>
            Aucun résultat pour votre recherche : <b><?= $r ?></b>
    <?php
        }
    ?>
    <br /><br />
    <a class="btn btn-primary btn-sm" href="javascript:history.go(-1)">Retour à l'Accueil</a>
    </div>
</body>
</html>
