<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('../cookieconnect.php');

if(isset($_GET['id']) AND !empty($_GET['id']))
{
    $get_id = htmlspecialchars($_GET['id']);

    $article = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
    $article->execute(array($get_id));

    if($article->rowCount() == 1)
    {
        $article = $article->fetch();
        $titre = $article['titre'];
        $contenu = $article['contenu'];
        $id = $article['id'];
        $prix = $article['prix'];
        $prix_occa = $article['prix_occasion'];
        $categories = $article['categories'];
        $pseudo = $article['pseudo'];
        $id_vendeur = $article['id_vendeur'];
        $date_time = $article['date_time_publi'];
    }
    else
    {
        die('L\'article que vous voulez voir n\'existe pas !');
    }
}
else
{
    header("Location: ../monprofil.php?id_membres=".$_SESSION['id_membres']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="page accueil.html">

    <title>Article : <?= $titre ?></title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark sticky-top">
    <h2><a class="navbar-brand" href="<?= "../monprofil.php?id_membres=".$_SESSION['id_membres'] ?>">TyuiopCase Menu</a></h2>
    <div class="nav-item">
        <h5><a class="nav-link" href="annonces.php">Mes Annonces</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="nav-link" href="addannonces.php">Ajouter des Annonces</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="nav-link" href="<?= "../login/profil.php?id_membres=".$_SESSION['id_membres'] ?>">Mon Profil</a></h5>
    </div>
    <div class="nav-item">
        <h5><a class="nav-link" href="../login/deconnexion.php">Se déconnecter</a></h5>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <nav id="menu" navigation-menu>
            <br />
            <nav class="navbar navbar-dark bg-primary"></nav>
            <br />
            <div align="center">
                <h4 style="color: deepskyblue">Les Catégories</h4>
            </div>
            <br />
            <ul class="topLevelMenu">
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="voiture.php" style="color: white">VOITURES</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="multimedia.php" style="color: white">MULTIMEDIAS</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                    <h5><a class="navItem Normal" href="vetement.php" style="color: white">VÊTEMENTS</a></h5>
                </li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li class="menuFooter">
                <h5><a class="navItem Normal" href="occasion.php" style="color: orange">OCCASIONS</a></h5>
                </li>
            </ul>
        </nav>
    </div>
</nav>
<br><br>
<div align="center">
    <h3><?= $article['titre'] ?></h3>
    Prix : <?= $article['prix'] ?> € --- Prix occasion : <?= $article['prix_occasion'] ?> €
    <br /><br />
    Description : <?= $article['contenu'] ?>
    <br /><br />
    <img src="miniatures/<?= $article['id'] ?>.jpg" width="100" />
    <br /><br />
    Annonce publié le : <?= $article['date_time_publi'] ?> par
    <?php 
        if ($article['id_vendeur'] != null) : 
    ?>
            <a href="../login/profil.php?id_membres=<?= $article['id_vendeur'] ?>"><b><?= $article['pseudo'] ?></b></a>
    <?php 
        else : 
    ?>
            <b><?= $article['pseudo'] ?></b>
    <?php 
        endif; 
    ?>.
    <br /><br />
    <a class="btn btn-primary btn-sm" href="occasion.php">Retour aux Articles</a>
    <br>
    </div>
</body>
</html>