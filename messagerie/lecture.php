<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('../cookieconnect.php');

if(isset($_SESSION['id_membres']) AND !empty($_SESSION['id_membres']))
{
    if(isset($_GET['id']) AND !empty($_GET['id']))
    {
        $id_msg = intval(($_GET['id']));

        $msg = $bdd->prepare("SELECT * FROM message WHERE id = ? AND id_destinataire = ?");
        $msg->execute(array($_GET['id'], $_SESSION['id_membres']));
        $msg_nbr = $msg->rowCount();
        $m = $msg->fetch();

        $email_exp = $bdd->prepare("SELECT email FROM membres WHERE id_membres = ?");
        $email_exp->execute(array($m['id_expediteur']));
        $email_exp = $email_exp->fetch();
        $email_exp = $email_exp['email'];

        ?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Lecture des messages #<?= $id_msg ?></title>
        </head>
        <body>
            
            <br />
            <a href="../login/profil.php?id_membres=<?= $_SESSION['id_membres'] ?>">Retour à mon Profil</a>
            &nbsp;&nbsp;&nbsp;
            <a href="reception.php">Ma Boîte de réception</a>
            &nbsp;&nbsp;&nbsp;
            <a href="envoi.php?r=<?= strtolower(urlencode($email_exp)) ?>&o=<?= urlencode($m['sujet']) ?>">Répondre au message</a>
            &nbsp;&nbsp;&nbsp;
            <a href="supprimer.php?id=<?= strtolower(urlencode($m['id'])) ?>">Supprimer le message</a>

            <br /><br />

            <h3 align="center">Lecture du message #<?= $id_msg ?></h3>

            <div align="center">
                <?php
                    if($msg_nbr == 0)
                    {
                        echo "Demi-tour : Ce message ne vous est pas destiné !";
                    }
                    else
                    {
                ?>
                        <b><?= $email_exp ?></b> vous a envoyé un message<br />
                        le <?= $m['date_time'] ?><br /><br />
                        <b>Sujet : </b><?= $m['sujet'] ?><br /><br />
                        <?= nl2br($m['message']) ?><br /><br />
                        ---------------------------------------
                        <br /><br />
                <?php
                    }
                ?>
                    
            </div>

        </body>
        </html>

<?php

        $lu = $bdd->prepare("UPDATE message SET lu = 1 WHERE id = ?");
        $lu->execute(array($m['id']));

    }
}

?>