<?php

session_start();

$bdd = new PDO('mysql:host=localhost;dbname=espace_membre', 'root', '');

include_once('../cookieconnect.php');

if(isset($_SESSION['id_membres']) AND !empty($_SESSION['id_membres']))
{
    if(isset($_GET['id']) AND !empty($_GET['id']))
    {
        $id_msg = intval(($_GET['id']));

        $msg = $bdd->prepare("DELETE FROM message WHERE id = ? AND id_destinataire = ?");
        $msg->execute(array($_GET['id'], $_SESSION['id_membres']));

        header("Location: reception.php");

    }
}

?>